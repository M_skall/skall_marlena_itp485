#pragma once
#include "engineMath.h"
#include "Job.h"

class Character;

class TestJob : public Job
{
public:
	TestJob(Character* character);
	~TestJob();
	void DoIt() override;
private:
	Character* mCharacter;
};