#pragma once
#include <thread>
#include <mutex>

class Worker
{
public:
	Worker();
	~Worker();
	void Begin();
	void End();
	static void Loop();

private:
	std::thread mThread;
	std::mutex worker_mutex;
};