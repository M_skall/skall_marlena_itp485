#include "Constants.hlsl"

#define MAX_SKELETON_BONES 80
cbuffer SkinConstants : register(b3)
{
    float4x4 c_skinMatrix[MAX_SKELETON_BONES];
};

struct VIn
{
    float3 position : POSITION0;
    float3 normal : NORMAL0;
    uint4 boneIndex : BONES0;
    float4 boneWeight : WEIGHTS0;
    float2 uv : TEXCOORD0;
};

struct VOut
{
    float4 position : SV_POSITION;
    float4 normal : NORMAL0;
    float2 uv : TEXCOORD0;
};

VOut VS(VIn vIn)
{
    VOut output;

#if 1
    float4 pos = float4(vIn.position, 1.0);
    vIn.position = mul(pos, c_skinMatrix[vIn.boneIndex.x]) * vIn.boneWeight.x
        + mul(pos, c_skinMatrix[vIn.boneIndex.y]) * vIn.boneWeight.y
        + mul(pos, c_skinMatrix[vIn.boneIndex.z]) * vIn.boneWeight.z
        + mul(pos, c_skinMatrix[vIn.boneIndex.w]) * vIn.boneWeight.w;
    float4 norm = float4(vIn.normal, 0.0);
    vIn.normal = mul(norm, c_skinMatrix[vIn.boneIndex.x]) * vIn.boneWeight.x
        + mul(norm, c_skinMatrix[vIn.boneIndex.y]) * vIn.boneWeight.y
        + mul(norm, c_skinMatrix[vIn.boneIndex.z]) * vIn.boneWeight.z
        + mul(norm, c_skinMatrix[vIn.boneIndex.w]) * vIn.boneWeight.w;
#endif

    // transform input position from model to world space
    float4 worldPos = mul(float4(vIn.position, 1.0), c_modelToWorld);
    // transform position from world to projection space
    output.position = mul(worldPos, c_viewProj);

    // transform input normal from model to world space
    output.normal = mul(float4(vIn.normal, 0.0), c_modelToWorld);

    output.uv = vIn.uv;
    return output;
}

float4 PS(VOut pIn) : SV_TARGET
{
	float4 diffuse = DiffuseTexture.Sample(DefaultSampler, pIn.uv);

	// do the lighting
	float3 lightColor = c_ambient;

	// when doing toon shading, don't multiply by lightColor again 
	//since c_ambient encases zero, so the values would never change 
	//instead, just setting the lightColor to one of three different values

	float3 n = normalize(pIn.normal.xyz);
	float3 l = normalize(c_dirLight.dir);
	float intensity = max(0.0, dot(n, l));
	if (intensity < 1.0 && intensity > 0.6)
	{
		lightColor = float3(1.0, 1.0, 1.0);
	}
	else if (intensity <= 0.6 && intensity > 0.3)
	{
		lightColor = float3(0.7, 0.7, 0.7);
	}
	else if (intensity <= 0.3 && intensity > 0.0)
	{
		lightColor = float3(0.3, 0.3, 0.3);
	}
	
    //TODO Fill This in

    float4 finalColor = diffuse * float4(lightColor, 1.0);
    return finalColor;
}
