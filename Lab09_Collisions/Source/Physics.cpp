#include "stdafx.h"
#include "Physics.h"
#include "Components\CollisionComponent.h"

struct TestAABB
{
	Physics::AABB a;
	Physics::AABB b;
	Physics::AABB overlap;
};
const TestAABB testAABB[] =
{
	{
		Physics::AABB(Vector3(0.0f, 0.0f, 0.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(0.0f, 0.0f, 0.0f), Vector3(10.0f, 10.0f, 10.0f)),
		Physics::AABB(Vector3(0.0f, 0.0f, 0.0f), Vector3(10.0f, 10.0f, 10.0f))
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-110.0f, -10.0f, -10.0f), Vector3(-90.0f, 10.0f, 10.0f)),
		Physics::AABB(Vector3(-100.0f, -10.0f, -10.0f), Vector3(-90.0f, 10.0f, 10.0f))
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(90.0f, -10.0f, -10.0f), Vector3(110.0f, 10.0f, 10.0f)),
		Physics::AABB(Vector3(90.0f, -10.0f, -10.0f), Vector3(100.0f, 10.0f, 10.0f))
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-10.0f, -110.0f, -10.0f), Vector3(10.0f, -90.0f, 10.0f)),
		Physics::AABB(Vector3(-10.0f, -100.0f, -10.0f), Vector3(10.0f, -90.0f, 10.0f))
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-10.0f, 90.0f, -10.0f), Vector3(10.0f, 110.0f, 10.0f)),
		Physics::AABB(Vector3(-10.0f, 90.0f, -10.0f), Vector3(10.0f, 100.0f, 10.0f))
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-10.0f, -10.0f, -110.0f), Vector3(10.0f, 10.0f, -90.0f)),
		Physics::AABB(Vector3(-10.0f, -10.0f, -100.0f), Vector3(10.0f, 10.0f, -90.0f))
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-10.0f, -10.0f, 90.0f), Vector3(10.0f, 10.0f, 110.0f)),
		Physics::AABB(Vector3(-10.0f, -10.0f, 90.0f), Vector3(10.0f, 10.0f, 100.0f))
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-120.0f, -10.0f, -10.0f), Vector3(-110.0f, 10.0f, 10.0f)),
		Physics::AABB(Vector3::One, Vector3::Zero)
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(110.0f, -10.0f, -10.0f), Vector3(120.0f, 10.0f, 10.0f)),
		Physics::AABB(Vector3::One, Vector3::Zero)
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-10.0f, -120.0f, -10.0f), Vector3(10.0f, -110.0f, 10.0f)),
		Physics::AABB(Vector3::One, Vector3::Zero)
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-10.0f, 110.0f, -10.0f), Vector3(10.0f, 120.0f, 10.0f)),
		Physics::AABB(Vector3::One, Vector3::Zero)
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-10.0f, -10.0f, -120.0f), Vector3(10.0f, -10.0f, -110.0f)),
		Physics::AABB(Vector3::One, Vector3::Zero)
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-10.0f, -10.0f, 110.0f), Vector3(10.0f, 10.0f, 120.0f)),
		Physics::AABB(Vector3::One, Vector3::Zero)
	},
};

Physics::Physics()
{

}

Physics::~Physics()
{

}

Physics::AABB::AABB()
{

}

Physics::AABB::AABB(Vector3 min, Vector3 max)
{
	mMin = min;
	mMax = max;
}

Physics::AABB::~AABB()
{

}

Physics::Ray::Ray()
{

}

Physics::Ray::~Ray()
{

}

bool Physics::Intersect(const AABB& a, const AABB& b, AABB* pOverlap)
{
	bool intersect = !(a.mMin.x > b.mMax.x ||
		b.mMin.x > a.mMax.x ||
		a.mMin.y > b.mMax.y ||
		b.mMin.y > a.mMax.y ||
		a.mMin.z > b.mMax.z ||
		b.mMin.z > a.mMax.z
		);

//	if (intersect == true)
	//{
		//max.x
		//if (Math::Min(abs(a.mMax.x), abs(b.mMax.x)) == abs(a.mMax.x))
		//{
		//	pOverlap->mMax.x = a.mMax.x;
		//}
		//else if(Math::Min(abs(a.mMax.x), abs(b.mMax.x)) == abs(b.mMax.x))
		//{
		//	pOverlap->mMax.x = b.mMax.x;
		//}
		////max.y
		//if (Math::Min(abs(a.mMax.y), abs(b.mMax.y)) == abs(a.mMax.y))
		//{
		//	pOverlap->mMax.y = a.mMax.y;
		//}
		//else if(Math::Min(abs(a.mMax.x), abs(b.mMax.x)) == abs(b.mMax.y))
		//{
		//	pOverlap->mMax.y = b.mMax.y;
		//}

		//// min.x
		//if (Math::Min(abs(a.mMin.x), abs(b.mMin.x)) == abs(a.mMin.x))
		//{
		//	pOverlap->mMin.x = a.mMin.x;
		//}
		//else if (Math::Min(abs(a.mMin.x), abs(b.mMin.x)) == abs(b.mMin.x))
		//{
		//	pOverlap->mMin.x = b.mMin.x;
		//}
		////min.y
		//if (Math::Min(abs(a.mMin.y), abs(b.mMin.y)) == abs(a.mMin.y))
		//{
		//	pOverlap->mMin.y = a.mMin.y;
		//}
		//else if(Math::Min(abs(a.mMin.y), abs(b.mMin.y)) == abs(b.mMin.y))
		//{
		//	pOverlap->mMin.y = b.mMin.y;
		//}

		////max.z
		//if (Math::Min(abs(a.mMax.x), abs(b.mMax.x)) == abs(a.mMax.x))
		//{
		//	pOverlap->mMax.x = a.mMax.x;
		//}
		//else if (Math::Min(abs(a.mMax.x), abs(b.mMax.x)) == abs(b.mMax.x))
		//{
		//	pOverlap->mMax.x = b.mMax.x;
		//}
		////max.z
		//if (Math::Min(abs(a.mMax.y), abs(b.mMax.y)) == abs(a.mMax.y))
		//{
		//	pOverlap->mMax.y = a.mMax.y;
		//}
		//else if (Math::Min(abs(a.mMax.x), abs(b.mMax.x)) == abs(b.mMax.y))
		//{
		//	pOverlap->mMax.y = b.mMax.y;
		//}

	if(intersect) {
		if (Math::Min(abs(a.mMax.x), abs(b.mMax.x)) == abs(a.mMax.x) ? pOverlap->mMax.x = a.mMax.x : pOverlap->mMax.x = b.mMax.x);
		if (Math::Min(abs(a.mMax.y), abs(b.mMax.y)) == abs(a.mMax.y) ? pOverlap->mMax.y = a.mMax.y : pOverlap->mMax.y = b.mMax.y);
		if (Math::Min(abs(a.mMax.z), abs(b.mMax.z)) == abs(a.mMax.z) ? pOverlap->mMax.z = a.mMax.z : pOverlap->mMax.z = b.mMax.z);
		if (Math::Min(abs(a.mMin.x), abs(b.mMin.x)) == abs(a.mMin.x) ? pOverlap->mMin.x = a.mMin.x : pOverlap->mMin.x = b.mMin.x);
		if (Math::Min(abs(a.mMin.y), abs(b.mMin.y)) == abs(a.mMin.y) ? pOverlap->mMin.y = a.mMin.y : pOverlap->mMin.y = b.mMin.y);
		if (Math::Min(abs(a.mMin.z), abs(b.mMin.z)) == abs(a.mMin.z) ? pOverlap->mMin.z = a.mMin.z : pOverlap->mMin.z = b.mMin.z);


		AABB* over = pOverlap;
		Vector3 val = over->mMax;
		Vector3 val2 = over->mMin;
	}
	else
	{
		pOverlap->mMax = Vector3::Zero;
		pOverlap->mMin = Vector3::Zero; // using Vector3::One should be here, 
										// but when it's loaded in the test struct, 
										// Vector3::One someonehow becomes (0.0,0.0,0.0)
	}

	// AABB collision
	return !(a.mMin.x > b.mMax.x || 
			b.mMin.x > a.mMax.x || 
			a.mMin.y > b.mMax.y || 
			b.mMin.y > a.mMax.y ||
			a.mMin.z > b.mMax.z ||
			b.mMin.z > a.mMax.z
			);

	//return false;
}

bool Physics::Intersect(const Ray& ray, const AABB& box, Vector3* pHitPoint)
{
	/*
	
	*/


	return false;
}

bool Physics::UnitTest()
{

	bool isOk = true;
	Vector3 one = Vector3::One;
	size_t size = sizeof(testAABB);
	// intersect AABB vs AABB
	{
		for (unsigned int i = 0; i < sizeof(testAABB)/sizeof(testAABB[0]); i++)
		{
			AABB overlap;
			Intersect(testAABB[i].a, testAABB[i].b, &overlap);
			//isOk &= Intersect(testAABB[i].a, testAABB[i].b, &overlap);
			isOk &= Math::IsCloseEnuf(overlap.mMin.x, testAABB[i].overlap.mMin.x);
			isOk &= Math::IsCloseEnuf(overlap.mMin.y, testAABB[i].overlap.mMin.y);
			isOk &= Math::IsCloseEnuf(overlap.mMin.z, testAABB[i].overlap.mMin.z);
			isOk &= Math::IsCloseEnuf(overlap.mMax.x, testAABB[i].overlap.mMax.x);
			isOk &= Math::IsCloseEnuf(overlap.mMax.y, testAABB[i].overlap.mMax.y);
			isOk &= Math::IsCloseEnuf(overlap.mMax.z, testAABB[i].overlap.mMax.z);
		}
		return isOk;
	}

	// intersect ray


	//Vector2 testVector(-1.0f, 0.0f);

	//// add 2 vectors
	//{
	//	Vector2 addVector = testVector + Vector2(1.0f, -1.0f);
	//	isOk &= Math::IsCloseEnuf(addVector.x, 0.0f);
	//	isOk &= Math::IsCloseEnuf(addVector.y, -1.0f);
	//	addVector = testVector;
	//	addVector += Vector2(0.0f, 2.0f);
	//	isOk &= Math::IsCloseEnuf(addVector.x, -1.0f);
	//	isOk &= Math::IsCloseEnuf(addVector.y, 2.0f);
	//}



	//return false;
}

void Physics::AddObj(CollisionComponent* pObj)
{
	mObj.push_back(pObj);
}

void Physics::RemoveObj(CollisionComponent* pObj)
{
	std::vector<CollisionComponent*>::iterator it = find(mObj.begin(), mObj.end(), pObj);
	if (it != mObj.end())
	{
		mObj.erase(it);
	}
}

static bool AABBCompare(const Physics::AABB& test, const Physics::AABB& orig)
{

	if (false == ( Math::IsCloseEnuf(test.mMin.x, orig.mMin.x) 
		|| Math::IsCloseEnuf(test.mMin.y, orig.mMin.y)
		|| Math::IsCloseEnuf(test.mMin.z, orig.mMin.z)))
	{
		return false;
	}

	if (false == (Math::IsCloseEnuf(test.mMax.x, orig.mMax.x)
		|| Math::IsCloseEnuf(test.mMax.y, orig.mMax.y)
		|| Math::IsCloseEnuf(test.mMax.z, orig.mMax.z)))
	{
		return false;
	}

	/*if (false == Math::IsCloseEnuf(test.mMin.y, orig.mMin.y))
	{
		return false;
	}

	if (false == Math::IsCloseEnuf(test.mMin.z, orig.mMin.y))
	{
		return false;
	}*/

	return true;
}