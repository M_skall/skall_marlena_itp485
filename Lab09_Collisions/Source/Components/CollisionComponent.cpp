#include "stdafx.h"
#include "CollisionComponent.h"
#include "game.h"
#include "jsonUtil.h"
#include "RenderObj.h"


CollisionComponent::CollisionComponent(RenderObj* pObj)
	: Component(pObj)
{
	mObj->GetGame()->mPhysics->AddObj(this);
}

CollisionComponent::~CollisionComponent()
{
	mObj->GetGame()->mPhysics->RemoveObj(this);
}

void CollisionComponent::LoadProperties(const rapidjson::Value& properties) override
{
	/*"position": [0.0, 650.0, 0.0],
		"rotation" : [0.0, 0.0, 0.0, 1.0],
		"scale" : 5.0,
		"mesh" : "Assets/Meshes/Platform.itpmesh2",
		"components" : [
	{
		"type": "Collision",
			"min" : [-50.0, -50.0, -1.0],
			"max" : [50.0, 50.0, 0.0]
	}*/
	Vector3 min;
	Vector3 max;
	GetVectorFromJSON(properties, "min", min);
	GetVectorFromJSON(properties, "max", max);
	mAABB.mMin = min;
	mAABB.mMax = max;
}

Physics::AABB CollisionComponent::GetAABB() const
{
	Physics::AABB aabb = mAABB;


	return aabb;
}