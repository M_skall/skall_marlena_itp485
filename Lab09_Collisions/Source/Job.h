#pragma once
#include "engineMath.h"

class Job
{
public:
	Job();
	~Job();
	virtual void DoIt();
};