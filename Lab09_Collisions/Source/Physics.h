#pragma once

#include "engineMath.h"

class CollisionComponent;

class Physics
{
public:
	class AABB
	{
		friend class Physics;
	public:
		AABB();
		AABB(Vector3 min, Vector3 max);
		~AABB();

		Vector3 mMin;
		Vector3 mMax;
	};
	class Ray // Physics::Ray is a line segment
	{
		friend class Physics;
	public:
		Ray();
		~Ray();
		Vector3 mFrom;
		Vector3 mTo;

	};
	Physics();
	~Physics();
	static bool Intersect(const AABB& a, const AABB& b, AABB* pOverlap = nullptr);
	static bool Intersect(const Ray& ray, const AABB& box, Vector3* pHitPoint = nullptr);
	static bool UnitTest();


	// TODO Lab 09h
	void AddObj(CollisionComponent* pObj);
	void RemoveObj(CollisionComponent* pObj);

private:

	// TODO Lab 09h
	std::vector<CollisionComponent*> mObj;
};