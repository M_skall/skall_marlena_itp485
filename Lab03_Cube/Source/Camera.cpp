#include "stdafx.h"
#include "Game.h"
#include "engineMath.h"
#include "Graphics.h"
#include "Shader.h"
#include "stringUtil.h"
#include "VertexBuffer.h"
#include "RenderObj.h"
#include "Camera.h"
#include <fstream>
#include <sstream>

Camera::Camera(class Graphics* pGraphics)
{
	mGraphics = pGraphics;
	mView = Matrix4::CreateTranslation(Vector3(0.0f, 0.0f, -100.0f));
	mProj = Matrix4::CreateOrtho(mGraphics->GetScreenHeight(), mGraphics->GetScreenHeight(), 1000.0f, -1000.0f);
	mCameraBuffer = mGraphics->CreateGraphicsBuffer(&mCameraData, sizeof(mCameraData), D3D11_BIND_CONSTANT_BUFFER, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);
}

void Camera::SetActive() 
{
	mCameraData.c_viewProj = mView * mProj;
	mView.Invert();
	mCameraData.c_cameraPosition = mView.GetTranslation();
	mGraphics->UploadBuffer(mCameraBuffer, &mCameraData, sizeof(mCameraData));
	mGraphics->GetDeviceContext()->VSSetConstantBuffers(Graphics::CONSTANT_BUFFER_CAMERA, 1, &mCameraBuffer);
	mGraphics->GetDeviceContext()->PSSetConstantBuffers(Graphics::CONSTANT_BUFFER_CAMERA, 1, &mCameraBuffer);
}