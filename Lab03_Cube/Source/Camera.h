#pragma once

class Camera
{
public:
	Camera(class Graphics* pGraphics);
	virtual ~Camera() 
	{
		mCameraBuffer->Release();
	}

	void SetActive();

	struct PerCameraConstants
	{
		Matrix4 c_viewProj;
		Vector3 c_cameraPosition;
		float padding;
	};

	Matrix4 mView; //the WorldToCamera matrix
	Matrix4 mProj; //the projection matrix

protected:	class Graphics* mGraphics;
	PerCameraConstants mCameraData;
	ID3D11Buffer* mCameraBuffer;

private:

};