#include "stdafx.h"
#include "Game.h"
#include "engineMath.h"
#include "Graphics.h"
#include "Shader.h"
#include "stringUtil.h"
#include "VertexBuffer.h"
#include <fstream>
#include <sstream>

VertexBuffer::VertexBuffer(class Graphics* graphics, const void* vertData, uint32_t vertCount, uint32_t vertStride,
	const void* indexData, uint32_t indexCount, uint32_t indexStride)
{
	mIndexCount = indexCount;
	mStrides = vertStride;
	mGraphics = graphics;
	if (indexStride == 2) {
		mIndexFormat = DXGI_FORMAT_R16_UINT;
	}
	else if (indexStride == 4) {
		mIndexFormat = DXGI_FORMAT_R32_UINT;
	}
	mBuffer = mGraphics->CreateGraphicsBuffer(vertData, vertCount * vertStride, D3D11_BIND_VERTEX_BUFFER, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);
	mIndexBuffer = mGraphics->CreateGraphicsBuffer(indexData, indexCount * indexStride, D3D11_BIND_INDEX_BUFFER, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);
}

VertexBuffer::~VertexBuffer()
{
	mBuffer->Release();
	mIndexBuffer->Release();
}

void VertexBuffer::SetActive() const 
{
	UINT offset = 0;
	mGraphics->GetDeviceContext()->IASetVertexBuffers(0, 1, &mBuffer, &mStrides, &offset);
	mGraphics->GetDeviceContext()->IASetIndexBuffer(mIndexBuffer, mIndexFormat, offset);
}

void VertexBuffer::Draw() const
{
	SetActive();
	mGraphics->GetDeviceContext()->DrawIndexed(mIndexCount, 0, 0);
}