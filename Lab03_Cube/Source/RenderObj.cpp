#include "stdafx.h"
#include "Game.h"
#include "engineMath.h"
#include "Graphics.h"
#include "Shader.h"
#include "stringUtil.h"
#include "VertexBuffer.h"
#include "RenderObj.h"
#include <fstream>
#include <sstream>

RenderObj::RenderObj(class Game* pGame, const class VertexBuffer* vertexBuffer, const class Shader* shader)
{
	mShader = shader;
	mVertexBuffer = vertexBuffer;
	mGraphics = pGame->GetGraphics();
	mObjectData.mModelToWorld = Matrix4::CreateRotationZ(Math::ToRadians(45.0f));
	mObjectBuffer = mGraphics->CreateGraphicsBuffer(&mObjectData, sizeof(mObjectData), D3D11_BIND_CONSTANT_BUFFER, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);
}

void RenderObj::Draw()
{
	//e Graphics::UploadBuffer()
	//	to update your mObjectBuffer with mObjectData and upload that to the GPU.
	//	o This effectively copies the �c_modelToWorld� matrix into the constant buffer

	mGraphics->UploadBuffer(mObjectBuffer, &mObjectData, sizeof(mObjectData));

	//ID3D11DeviceContext::VSSetConstantBuffers() to bind your mObjectBuffer.
	//	o As you can see in Constants.hlsl, we�ve got �c_modelToWorld� in constant buffer slot
	//	�b1�.That means you�ll want to upload your constant buffer to slot index 1. There is a
	//	constant declared in Graphics.h �CONSTANT_BUFFER_RENDEROBJ� for this purpose.

	mGraphics->GetDeviceContext()->VSSetConstantBuffers(Graphics::CONSTANT_BUFFER_RENDEROBJ, 1, &mObjectBuffer);

	mShader->SetActive();
	mVertexBuffer->Draw();
}


