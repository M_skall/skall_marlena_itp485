#pragma once

class VertexBuffer
{
public:
	VertexBuffer(class Graphics* graphics, const void* vertData, uint32_t vertCount, uint32_t vertStride,
		const void* indexData, uint32_t indexCount, uint32_t indexStride
	);
	~VertexBuffer();
	void SetActive() const;
	void Draw() const;

	ID3D11Buffer* GetBuffer() { return mBuffer; }

private:
	ID3D11Buffer* mBuffer;
	UINT mStrides;
	uint32_t mVertexCount;
	uint32_t mIndexCount;
	class Graphics* mGraphics;
	ID3D11Buffer* mIndexBuffer;
	DXGI_FORMAT mIndexFormat;

};