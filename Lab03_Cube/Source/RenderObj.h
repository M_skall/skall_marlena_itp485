#pragma once

class RenderObj
{
public:
	RenderObj(class Game* pGame, const class VertexBuffer* vertexBuffer, const class Shader* shader);
	virtual ~RenderObj() {
		delete mVertexBuffer;
		mObjectBuffer->Release();
	}
	virtual void Draw();

	struct PerObjectConstants
	{
		Matrix4 mModelToWorld;
	};

	void SetPerObjConst(Matrix4 objConst) { mObjectData.mModelToWorld = objConst; }
	Matrix4 GetPerObjConst() { return mObjectData.mModelToWorld; }


private:
	class Graphics* mGraphics;
	const class VertexBuffer* mVertexBuffer;
	const class Shader* mShader;
	PerObjectConstants mObjectData;
	ID3D11Buffer* mObjectBuffer;

};