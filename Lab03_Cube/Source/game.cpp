﻿#include "stdafx.h"
#include "Game.h"
#include "engineMath.h"
#include "Graphics.h"
#include "Shader.h"
#include "VertexBuffer.h"
#include "RenderObj.h"
#include "RenderCube.h"
#include "Camera.h"
#include "stringUtil.h"
#include "rapidjson\include\rapidjson\rapidjson.h"
#include "rapidjson\include\rapidjson\document.h"
#include <fstream>
#include <sstream>


// TODO Lab 02e
struct VertexPosColor
{
	Vector3 pos;
	Graphics::Color4 color;
};

float globalRotation = 0.0f;

Game::Game()
{
}

Game::~Game()
{
}

void Game::Init(HWND hWnd, float width, float height)
{
	// TODO Lab 02b
	mGraphics.InitD3D(hWnd, width, height);

	// TODO Lab 02e
	//vertex buffer with triangle data in it
	VertexPosColor vert[] =
	{
		{ Vector3(0.0f, 0.5f, 0.0f), Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f) },
		{ Vector3(0.45f, -0.5, 0.0f), Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f) },
		{ Vector3(-0.45f, -0.5f, 0.0f), Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f) }
	};
	vertsize = sizeof(vert[0]);

	// add an array of uint16_t for your triangle indices
	static uint16_t triangleIndex[] =
	{
		0, 1, 2
	};


	static VertexPosColor cubeVertex[] =
	{
		//first face - back face
		{ Vector3(-0.5f, 0.5f, -0.5f), Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f) }, //0
		{ Vector3(0.5f, 0.5f, -0.5f), Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f) }, //1
		{ Vector3(0.5f, -0.5f, -0.5f), Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f) }, //2
		{ Vector3(-0.5f, -0.5f, -0.5f), Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f) }, //3

		//fourth face - left side
		{ Vector3(-0.5f, 0.5f, 0.5f), Graphics::Color4(0.0f, 1.0f, 1.0f, 1.0f) }, //4
		{ Vector3(-0.5f, 0.5f, -0.5f), Graphics::Color4(0.0f, 1.0f, 1.0f, 1.0f) }, //5
		{ Vector3(-0.5f, -0.5f, -0.5f), Graphics::Color4(0.0f, 1.0f, 1.0f, 1.0f) }, //6
		{ Vector3(-0.5f, -0.5f, 0.5f), Graphics::Color4(0.0f, 1.0f, 1.0f, 1.0f) }, //7
		
		//second face - front
		{ Vector3(0.5f, 0.5f, 0.5f), Graphics::Color4(1.0f, 0.0f, 1.0f, 1.0f) }, //8
		{ Vector3(-0.5f, 0.5f, 0.5f), Graphics::Color4(1.0f, 0.0f, 1.0f, 1.0f) }, //9
		{ Vector3(-0.5f, -0.5f, 0.5f), Graphics::Color4(1.0f, 0.0f, 1.0f, 1.0f) }, //10
		{ Vector3(0.5f, -0.5f, 0.5f), Graphics::Color4(1.0f, 0.0f, 1.0f, 1.0f) }, //11
		
		//third face - right 
		{ Vector3(0.5f, 0.5f, -0.5f), Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f) }, //12
		{ Vector3(0.5f, 0.5f, 0.5f), Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f) }, //13
		{ Vector3(0.5f, -0.5f, 0.5f), Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f) }, //14
		{ Vector3(0.5f, -0.5f, -0.5f), Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f) }, //15
		
		//sixth face - top
		{ Vector3(0.5f, 0.5f, -0.5f), Graphics::Color4(0.0f, 1.0f, 0.0f, 0.0f) }, //16
		{ Vector3(-0.5f, 0.5f, -0.5f), Graphics::Color4(0.0f, 1.0f, 0.0f, 0.0f) }, //17
		{ Vector3(-0.5f, 0.5f, 0.5f), Graphics::Color4(0.0f, 1.0f, 0.0f, 0.0f) }, //18
		{ Vector3(0.5f, 0.5f, 0.5f), Graphics::Color4(0.0f, 1.0f, 0.0f, 0.0f) }, //19
		
		
		//fifth face - bottom
		{ Vector3(-0.5f, -0.5f, -0.5f), Graphics::Color4(1.0f, 1.0f, 0.0f, 1.0f) }, //22
		{ Vector3(0.5f, -0.5f, -0.5f), Graphics::Color4(1.0f, 1.0f, 0.0f, 1.0f) }, //23
		{ Vector3(0.5f, -0.5f, 0.5f), Graphics::Color4(1.0f, 1.0f, 0.0f, 1.0f) }, //21
		{ Vector3(-0.5f, -0.5f, 0.5f), Graphics::Color4(1.0f, 1.0f, 0.0f, 1.0f) } //20
		
	};
	static uint16_t cubeIndex[] =
	{
		2, 1, 0, //back
		3, 2, 0,

		6, 5, 4, //left
		7, 6, 4,

		
		10, 9, 8, // front
		11, 10, 8,

		
		14, 13, 12, //right
		15, 14, 12,
		
		18, 17, 16,  //top
		19, 18, 16,
		
		22, 21, 20, //bottom
		23, 22, 20
		
	};

	//lab 03b -> to test that CreateGraphicsBuffer still produced triangle
	//mTriangleVert = mGraphics.CreateGraphicsBuffer(vert, sizeof(vert), D3D11_BIND_VERTEX_BUFFER, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);
	//lab 03c -> to test that creating VertexBuffer and replacing mTriangleVert still produced triangle
	VertexBuffer* triVertexBuffer = new VertexBuffer(&mGraphics, vert, 3, sizeof(VertexPosColor), triangleIndex, 3, sizeof(uint16_t));
	//24 vertices, 36 indices
	VertexBuffer* cubeVertexBuffer = new VertexBuffer(&mGraphics, cubeVertex, 24, sizeof(VertexPosColor), cubeIndex, 36, sizeof(uint16_t));

	//load shader
	mSimpleShader = new Shader(GetGraphics());

	mTriangleObj = new RenderObj(this, triVertexBuffer, mSimpleShader);
	mCubeObj = new RenderCube(this, cubeVertexBuffer, mSimpleShader);

	D3D11_INPUT_ELEMENT_DESC inputElem[] =
	{
	 { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosColor, pos),
	D3D11_INPUT_PER_VERTEX_DATA, 0 },
	 { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(VertexPosColor, color),
	D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	const wchar_t* fileName = L"Shaders/Mesh.hlsl";
	mSimpleShader->Load(fileName, inputElem, 2);

	mCamera = new Camera(&mGraphics);
}

void Game::Shutdown()
{
	// TODO Lab 02b
	//mGraphics.CleanD3D();

	// TODO Lab 02e
	//mTriangleVert->Release();

	// TODO Lab 02f
	delete mSimpleShader;

	delete mCubeObj;

	delete mTriangleObj;

	delete mCamera;

	mGraphics.CleanD3D();

}

void Game::Update(float deltaTime)
{
	globalRotation += Math::Pi * deltaTime;
	//mTriangleObj->SetPerObjConst(Matrix4::CreateRotationZ(globalRotation));
	mTriangleObj->SetPerObjConst(Matrix4::CreateScale(300.0f) * Matrix4::CreateRotationZ(globalRotation));

	//Matrix4::CreateScale(100.0f) * Matrix4::CreateRotationY(globalRotation) * Matrix4::CreateRotationX(0.25f*globalRotation);


	mCubeObj->SetPerObjConst(Matrix4::CreateScale(100.0f) * Matrix4::CreateRotationY(globalRotation) * Matrix4::CreateRotationX(0.25f*globalRotation));
}

void Game::RenderFrame()
{
	// TODO Lab 02b
	mGraphics.BeginFrame(Graphics::Color4(0.0f, 0.2f, 0.4f, 1.0f));

	// TODO Lab 02g
	mCamera->SetActive();
	mTriangleObj->Draw();
	mCubeObj->Draw();
	//mSimpleShader->SetActive();
	//UINT offset = 0;
	//mGraphics.GetDeviceContext()->IASetVertexBuffers(0, 1, &mTriangleVert, &vertsize, &offset);
	//ID3D11Buffer* buff = mVertexBuffer->GetBuffer();
	//mGraphics.GetDeviceContext()->IASetVertexBuffers(0, 1, &buff, &vertsize, &offset);
	//mGraphics.GetDeviceContext()->Draw(3, 0);

	mGraphics.EndFrame();

}

void Game::OnKeyDown(uint32_t key)
{
	m_keyIsHeld[key] = true;
}

void Game::OnKeyUp(uint32_t key)
{
	m_keyIsHeld[key] = false;
}

bool Game::IsKeyHeld(uint32_t key) const
{
	const auto find = m_keyIsHeld.find(key);
	if (find != m_keyIsHeld.end())
		return find->second;
	return false;
}

bool Game::LoadLevel(const WCHAR* fileName)
{
	std::ifstream file(fileName);
	if (!file.is_open())
	{
		return false;
	}

	std::stringstream fileStream;
	fileStream << file.rdbuf();
	std::string contents = fileStream.str();
	rapidjson::StringStream jsonStr(contents.c_str());
	rapidjson::Document doc;
	doc.ParseStream(jsonStr);

	if (!doc.IsObject())
	{
		return false;
	}

	std::string str = doc["metadata"]["type"].GetString();
	int ver = doc["metadata"]["version"].GetInt();

	// Check the metadata
	if (!doc["metadata"].IsObject() ||
		str != "itplevel" ||
		ver != 2)
	{
		return false;
	}

	return true;
}