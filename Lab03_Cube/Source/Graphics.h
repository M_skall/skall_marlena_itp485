#pragma once


class Graphics
{
public:
    enum TextureSlot
    {
        TEXTURE_SLOT_DIFFUSE,

		TEXTURE_SLOT_TOTAL
    };

	enum ConstantBuffer
	{
		CONSTANT_BUFFER_CAMERA,
		CONSTANT_BUFFER_RENDEROBJ,
        CONSTANT_BUFFER_LIGHTING,
		CONSTANT_BUFFER_SKINNING,
	};

    class Color4
    {
    public:
        Color4()
            : r(0), g(0), b(0), a(0)
        {}
        Color4(float _r, float _g, float _b, float _a = 1.0f)
            : r(_r), g(_g), b(_b), a(_a)
        {}
        float r, g, b, a;
    };

    Graphics();
    ~Graphics();

    void InitD3D(HWND hWnd, float width, float height);
    void BeginFrame(const Color4 &clearColor);
    void EndFrame();
    void CleanD3D();

	float GetScreenWidth() const { return mScreenWidth; }
	float GetScreenHeight() const { return mScreenHeight; }

	// TODO Lab 02c
	ID3D11Device* GetDevice() { return mDev; }
	ID3D11DeviceContext* GetDeviceContext() { return mDevcon; }

	// TODO Lab 03b
	//simplfy the process of updating graphics buffers
	void UploadBuffer(ID3D11Buffer* buffer, const void* data, size_t dataSize);
	//simplify the process of creating graphics buffers
	ID3D11Buffer* CreateGraphicsBuffer(const void *initialData, int inDataSize, 
		D3D11_BIND_FLAG inBindFlags, D3D11_CPU_ACCESS_FLAG inCPUAccessFlags, D3D11_USAGE inUsage);

private:
    float mScreenWidth;
    float mScreenHeight;

	// TODO Lab 02c
	IDXGISwapChain* mSwapchain;
	ID3D11Device* mDev;
	ID3D11DeviceContext* mDevcon;

	// TODO Lab 02d
	ID3D11RenderTargetView* mBackbuffer;
};