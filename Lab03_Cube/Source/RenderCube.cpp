#include "stdafx.h"
#include "Game.h"
#include "engineMath.h"
#include "Graphics.h"
#include "Shader.h"
#include "stringUtil.h"
#include "VertexBuffer.h"
#include "RenderCube.h"
#include "Camera.h"
#include <fstream>
#include <sstream>

RenderCube::RenderCube(class Game* pGame, const class VertexBuffer* vertexBuffer, const class Shader* shader)
	: RenderObj(pGame, vertexBuffer, shader)
{
}

RenderCube::~RenderCube() 
{

}
