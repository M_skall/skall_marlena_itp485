// Vector_01.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "engineMath.h"
#include "engineMemory.h"
#include "simdMath.h"
#include <assert.h>

struct TestStruct
{
    SIMDVector3 v1;
    float f1;
    char c1;
    SIMDMatrix4 m1;
    float f2;
    SIMDVector3 v2;

    static bool CheckAddress(void *address)
    {
        uintptr_t intAddress = (uintptr_t)address;
        if (intAddress & 0xf)
        {
            return false;
        }
        return true;
    }

    bool CheckIt()
    {
        bool isOk = CheckAddress(&v1) & CheckAddress(&m1) & CheckAddress(&v2);
        assert(isOk);
        return isOk;
    }

#if 1   // TODO Lab 01k
    static void* operator new(std::size_t sz)
    {
        return AlignendMalloc(sz, 16);
    }
    static void* operator new[](std::size_t sz)
    {
        return AlignendMalloc(sz, 16);
    }
    static void operator delete(void* ptr, std::size_t sz)
    {
        AlignedFree(ptr);
    }
    static void operator delete[](void* ptr, std::size_t sz)
    {
        AlignedFree(ptr);
    }
#endif
};

int main()
{
    bool isOk = true;

    isOk &= Vector2::UnitTest();
    assert(isOk);
    isOk &= Vector3::UnitTest();
    assert(isOk);
    isOk &= Vector4::UnitTest();
    assert(isOk);
    isOk &= Matrix3::UnitTest();
    assert(isOk);
    isOk &= Matrix4::UnitTest();
    assert(isOk);
    isOk &= Quaternion::UnitTest();
    assert(isOk);

#if 1   // TODO Lab 01f
    isOk &= SIMDVector3::UnitTest();
    assert(isOk);
    isOk &= SIMDVector4::UnitTest();
    assert(isOk);
    isOk &= SIMDMatrix4::UnitTest();
    assert(isOk);
#endif

#if 1   // TODO Lab 01j
    isOk &= AlignedMalloc_UnitTest();
    assert(isOk);
#endif

#if 1   // TODO Lab 01i
    for (int i = 0; i < 100; ++i)
    {
        TestStruct* pointers[100];
        for (int j = 0; j < 100; j++)
        {
            pointers[j] = new TestStruct;
            isOk &= pointers[j]->CheckIt();
        }
        for (int j = 0; j < 100; j++)
        {
            delete pointers[j];
        }
    }
#endif

    return (int)(false == isOk);	// return 0 if everything is ok
}

