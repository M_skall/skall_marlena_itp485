#pragma once

#include "engineMath.h"
#include "BoneTransform.h"
#include <algorithm>

class Game;

class Skeleton
{
	struct Bone 
	{
		BoneTransform mLocalBindPose;
		std::string mName;
		int mParent;
	};
public:
	Skeleton();
	~Skeleton();

	size_t GetNumBones() const { return mBones.size();  }
	const Bone& GetBone(size_t idx) const { return mBones[idx]; }
	const std::vector<Bone>& GetBones() const { return mBones; }
	const std::vector<Matrix4>& GetGlobalInvBindPoses() const { return mGlobalInvBindPoses; }

	static Skeleton* StaticLoad(const WCHAR* fileName, Game* pGame);
	bool Load(const WCHAR* fileName);

private:
	void ComputeGlobalInvBindPose();

	std::vector<Bone> mBones;
	std::vector<Matrix4> mGlobalInvBindPoses;
};