﻿#include "stdafx.h"
#include "Character.h"
#include "jsonUtil.h"
#include "RenderObj.h"
#include "Skeleton.h"
#include "SkinnedObj.h"
#include "Animation.h"
#include "jsonUtil.h"
#include "stringUtil.h"
#include "rapidjson\include\rapidjson\rapidjson.h"
#include "rapidjson\include\rapidjson\document.h"

Character::Character(RenderObj* pObj)
	: Component(pObj), mAnimationTime(0.0)
{
	mSkinnedObj = reinterpret_cast<SkinnedObj*>(pObj);
}

Character::~Character()
{
}

void Character::LoadProperties(const rapidjson::Value& properties)
{
	//need to add components - look at Assets/Levels/Level07
	std::wstring skeleton;
	GetWStringFromJSON(properties, "skeleton", skeleton);
	mSkeleton = Skeleton::StaticLoad(skeleton.c_str(), mObj->GetGame());

	const rapidjson::Value& animations = properties["animations"];
	if (animations.IsArray())
	{
		for (rapidjson::SizeType i = 0; i < animations.Size(); ++i)
		{
			//std::unordered_map<std::string, const Animation*> mAnims; -> [short name, file name]
			/*
			Each element of the array is itself a pair of strings [short name, file name].
			▪ Load each animation, and enter it into the mAnims map with the short name as
			the key
			*/
			std::string shortname;
			std::string filename;
			std::wstring f;

			const rapidjson::Value& names = animations[i];
			if (names.IsArray())
			{
				shortname = names[0].GetString();
				filename = names[1].GetString();
				StringUtil::String2WString(f, filename);
			}
			Animation* anim = Animation::StaticLoad(f.c_str(), mObj->GetGame());
			mAnims.insert(std::make_pair(shortname, anim));
		}
	}

}

bool Character::SetAnim(const std::string& animName)
{
	auto iter = mAnims.find(animName);
	if (iter != mAnims.end()) 
	{
		mCurrentAnim = iter->second;
		mAnimationTime = 0.0;
		return true;
	}
	mCurrentAnim = nullptr;
	return false;
}

/*
Advance mAnimationTime by deltaTime.
o Be careful to wrap mAnimationTime so that it does not exceed Animation::GetLength()
for the current animation (if there is one).
o If the time does exceed the length of the current animation, wrap the time back around
to 0.0 to make the animation loop. (Don’t just set it to 0.0, wrap it around.)
o Call Animation::GetGlobalPoseAtTime()
o Combine the resulting matrices with the inverse bind pose matrices from
Skeleton::GetGlobalInvBindPoses().
o Copy the combined matrices into the skinning matrix buffer
*/
void Character::UpdateAnim(float deltaTime)
{
	mAnimationTime += deltaTime;
	if (mAnimationTime >= mCurrentAnim->GetLength())
	{
		mAnimationTime -= mCurrentAnim->GetLength();
	}
	/*if (mAnimationTime < 0.0f)
	{
		mAnimationTime += mCurrentAnim->GetLength();
	}*/
	std::vector<Matrix4> globalPoses;
	mCurrentAnim->GetGlobalPoseAtTime(globalPoses, mSkeleton, mAnimationTime);
	std::vector<Matrix4> inverseBindPoses = mSkeleton->GetGlobalInvBindPoses();
	std::vector<Matrix4> combined;
	for (unsigned int i = 0; i < globalPoses.size(); i++)
	{
		combined.push_back(inverseBindPoses[i] * globalPoses[i]);
	}
	memcpy(mSkinnedObj->mSkinConstants.c_skinMatrix, combined.data(), sizeof(mSkinnedObj->mSkinConstants));
	//mObj->GetGame()->GetGraphics()->UploadBuffer(mSkinnedObj->mSkinningBuffer, &mSkinnedObj->mSkinConstants, sizeof(mSkinnedObj->mSkinConstants));
}

void Character::Update(float deltaTime)
{
	if (mCurrentAnim == nullptr)
	{
		SetAnim("idle");
	}
	UpdateAnim(deltaTime);
}