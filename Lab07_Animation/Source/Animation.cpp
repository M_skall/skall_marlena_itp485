﻿#include "stdafx.h"
#include "Animation.h"
#include "BoneTransform.h"
#include "Skeleton.h"
#include "game.h"
#include "rapidjson\include\rapidjson\rapidjson.h"
#include "rapidjson\include\rapidjson\document.h"
#include <fstream>
#include <sstream>
#include "jsonUtil.h"

Animation::Animation()
{

}

Animation::~Animation()
{

}

/*
Loop through all the bones in mTracks
▪ Some bones have no animation on them in which case mTracks[index] will have
a length of 0.
▪ If mTracks[index] has anything in it, it will have the entire animation sequence
with mNumFrames elements.
▪ If mTracks[index] is empty, use an identity matrix for that bone.
▪ If mTracks[index] is NOT empty, just use the first frame mTracks[index][0] for
now. Convert the BoneTransform to a Matrix with BoneTransform::ToMatrix().
▪ Once you have the bone’s matrix, combine that with its parent’s matrix if there
is one. The parent index will be set to -1 if there is no parent.
▪ When you have the final combined matrix, copy that into outPoses.
*/

void Animation::GetGlobalPoseAtTime(std::vector<Matrix4>& outPoses, const Skeleton *inSkeleton, float inTime) const
{
	// TODO Lab 07g
	/*
	 to choose the keyframe closest to the current inTime.
	o inTime is in seconds.
	o Use Animation::GetLength() to get the duration of the animation in seconds.
	o Use Animation::GetNumFrames() to get the number of frames in the animation.
	o GetLength() / (GetNumFrames() - 1) is the time per frame.
	*/
	float timePerFrame = GetLength() / (GetNumFrames() - 1);
	//int time = (int)timePerFrame;
	//float frame = inTime / timePerFrame;
	//int currFrame = (int)frame;
	float frac = inTime / timePerFrame; // ex. frame 0 (0.0), frame 1 (0.1)
	int currFrame = (int)frac;
	int nextFrame = currFrame + 1; //go up a frame
	float lerpF = frac - currFrame; //between frames, takes fraction

	// TODO Lab 07f
	// loop through all bones in mTracks
	for (unsigned int i = 0; i < mTracks.size(); i++)
	{
		//if (mTracks[i].size() != 0)
		//{
		//	Matrix4 mat = mTracks[i][currFrame].ToMatrix();
		//	//if (i > 0) {
		//		if (inSkeleton->GetBone(i).mParent != -1)
		//		{
		//			//int mParentIndex = inSkeleton->GetBone(i).mParent;
		//			//mat *= inSkeleton->GetBone(mParentIndex).mLocalBindPose.ToMatrix();
		//			//mat *= mTracks[inSkeleton->GetBone(i).mParent][0].ToMatrix();
		//			mat *= outPoses[inSkeleton->GetBone(i).mParent];
		//		}
		//	//}
		//	outPoses.push_back(mat);
		//}
		//else
		//{
		//	outPoses.push_back(Matrix4::Identity);
		//}
	
		if (mTracks[i].size() != 0)
		{
			//Matrix4 mat = mTracks[i][currFrame].ToMatrix();
			BoneTransform bt = BoneTransform::Interpolate(mTracks[i][currFrame], mTracks[i][nextFrame], lerpF);
			Matrix4 mat = bt.ToMatrix();

			//if (i > 0) {
			if (inSkeleton->GetBone(i).mParent != -1)
			{
				//int mParentIndex = inSkeleton->GetBone(i).mParent;
				//mat *= inSkeleton->GetBone(mParentIndex).mLocalBindPose.ToMatrix();
				//mat *= mTracks[inSkeleton->GetBone(i).mParent][0].ToMatrix();
				mat *= outPoses[inSkeleton->GetBone(i).mParent];
			}
			//}
			outPoses.push_back(mat);
		}
		else
		{
			outPoses.push_back(Matrix4::Identity);
		}
	
	}
}


Animation* Animation::StaticLoad(const WCHAR* fileName, Game* pGame)
{
	Animation* pAnimation = new Animation();
	if (false == pAnimation->Load(fileName))
	{
		delete pAnimation;
		return nullptr;
	}
	return pAnimation;
}

bool Animation::Load(const WCHAR* fileName)
{
	std::ifstream file(fileName);
	if (!file.is_open())
	{
		return false;
	}

	std::stringstream fileStream;
	fileStream << file.rdbuf();
	std::string contents = fileStream.str();
	rapidjson::StringStream jsonStr(contents.c_str());
	rapidjson::Document doc;
	doc.ParseStream(jsonStr);

	if (!doc.IsObject())
	{
		return false;
	}

	std::string str = doc["metadata"]["type"].GetString();
	int ver = doc["metadata"]["version"].GetInt();

	// Check the metadata
	if (!doc["metadata"].IsObject() ||
		str != "itpanim" ||
		ver != 2)
	{
		return false;
	}

	// Sequence
	const rapidjson::Value& sequence = doc["sequence"];
	int frames;
	float length;
	int bonecount;
	if (!sequence.IsObject())
	{
		return false;
	}
	GetIntFromJSON(sequence, "frames", frames);
	GetFloatFromJSON(sequence, "length", length);
	GetIntFromJSON(sequence, "bonecount", bonecount);

	mNumFrames = frames;
	mLength = length;
	mNumBones = bonecount;

	const rapidjson::Value& tracks = sequence["tracks"];
	int bone;
	Quaternion rot;
	Vector3 trans;
	//mTracks.resize(mNumBones);
	for (rapidjson::SizeType i = 0; i < tracks.Size(); i++)
	{
		if (!tracks.IsArray())
		{
			return false;
		}
		GetIntFromJSON(tracks[i], "bone", bone);
		while (i != bone) {
			mTracks.push_back(std::vector<BoneTransform>());
			i++;
		}

		const rapidjson::Value& transforms = tracks[i]["transforms"];
		std::vector<BoneTransform> vectBoneTrans;
		for (rapidjson::SizeType j = 0; j < transforms.Size(); j++)
		{
			BoneTransform bonetrans;
			if (!transforms.IsArray())
			{
				return false;
			}
			GetQuaternionFromJSON(transforms[j], "rot", rot);
			GetVectorFromJSON(transforms[j], "trans", trans);
			bonetrans.mRotation = rot;
			bonetrans.mTranslation = trans;

			vectBoneTrans.push_back(bonetrans);
		}
		mTracks.push_back(vectBoneTrans);
	} //end outer for

	while (mTracks.size() < mNumBones)
	{
		mTracks.push_back(std::vector<BoneTransform>());
	}
	return true;
}
