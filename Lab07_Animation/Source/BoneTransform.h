#pragma once
#include "stdafx.h"
#include "engineMath.h"

class BoneTransform
{
public:
	Quaternion mRotation;
	Vector3 mTranslation;
	Matrix4 ToMatrix() const
	{
		return Matrix4::CreateFromQuaternion(mRotation) * Matrix4::CreateTranslation(mTranslation);
	}

	// TODO Lab 07h
	static BoneTransform Interpolate(const BoneTransform& a, const BoneTransform& b, float f)
	{
		BoneTransform bt;
		bt.mRotation = Slerp(a.mRotation, b.mRotation, f);
		bt.mTranslation = Lerp(a.mTranslation, b.mTranslation, f);
		
		return bt;
	}

private:

};