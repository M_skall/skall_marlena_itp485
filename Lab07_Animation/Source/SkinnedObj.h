#pragma once
#include "engineMath.h"
#include "Graphics.h"
#include "RenderObj.h"

class Component;
class Game;
class Mesh;
class Shader;
class Texture;
class VertexBuffer;

#define MAX_SKELETON_BONES 80

class SkinnedObj : public RenderObj
{
public:
	struct SkinConstants
	{
		Matrix4 c_skinMatrix[MAX_SKELETON_BONES];
	};
	SkinnedObj(Game* pGame, const Mesh* pMesh);
	~SkinnedObj();
	void Draw() override;

	SkinConstants mSkinConstants;
	ID3D11Buffer* mSkinningBuffer;

protected:
	Game* mGame;
};
