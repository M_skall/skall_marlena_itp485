﻿#include "stdafx.h"
#include "Skeleton.h"
#include "game.h"
#include "rapidjson\include\rapidjson\rapidjson.h"
#include "rapidjson\include\rapidjson\document.h"
#include <fstream>
#include <sstream>
#include "jsonUtil.h"

Skeleton::Skeleton()
{

}

Skeleton::~Skeleton()
{

}

Skeleton* Skeleton::StaticLoad(const WCHAR* fileName, Game* pGame)
{
	Skeleton* pSkeleton = new Skeleton();
	if(false == pSkeleton->Load(fileName))
	{
		delete pSkeleton;
		return nullptr;
	}
	return pSkeleton;
}

/*
Loop through all the bones calculating the Bind Pose of each.
▪ If the bone has a parent, you must multiply the Bind Pose of the child bone by
its parent bone and its parent bone all the way up the chain.
▪ The bones have been arranged such that parent bones always come before their
children.
▪ If you go through the bones in order, you end up combining the matrices all the
way from the root up to the leaf without having to iterate back up the chain
each time.
o Loop through all the bones inverting the Bind Pose of each.

*/

void Skeleton::ComputeGlobalInvBindPose()
{
	for (unsigned int i = 0; i < mBones.size(); i++)
	{
		Matrix4 mat = mBones[i].mLocalBindPose.ToMatrix();
		//if (i > 0) {
			//int index = mBones[i].mParent;
			if (mBones[i].mParent != -1) // bone has parent
			{
				//int parent = mBones[index].mParent;
				mat *= mGlobalInvBindPoses[mBones[i].mParent];
				//index = mBones[index].mParent;
			}
		//}
		mGlobalInvBindPoses.push_back(mat);
	}

	//invert matrices
	for (unsigned int i = 0; i < mGlobalInvBindPoses.size(); i++)
	{
		mGlobalInvBindPoses[i].Invert();
	}

}

bool Skeleton::Load(const WCHAR* fileName)
{
	//set mbones
	/*
	We’ll be loading “Assets/Anims/SK_Mannequin.itpskel”. It’s a good idea to look at the
	file as you try to write the code to parse it.
	o This begins easily enough. Imitate Mesh::Load().
	o The number of bones in the skeleton are found as "bonecount"
	o Next follows the array of bones as "bones"
	▪ "name" is obvious enough
	▪ "parent" is the index of the parent bone
	▪ "bindpose" comes next with members:
	• "rot" is the quaternion for the bind pose
	• "trans" is the position for the bind pose
	• We are not supporting any bone scale
	o Once you’ve loaded all the Bones, call ComputeGlobalInvBindPose()

	*/

	std::ifstream file(fileName);
	if (!file.is_open())
	{
		return false;
	}
	
	std::stringstream fileStream;
	fileStream << file.rdbuf();
	std::string contents = fileStream.str();
	rapidjson::StringStream jsonStr(contents.c_str());
	rapidjson::Document doc;
	doc.ParseStream(jsonStr);

	if (!doc.IsObject())
	{
		return false;
	}

	std::string str = doc["metadata"]["type"].GetString();
	int ver = doc["metadata"]["version"].GetInt();

	// Check the metadata
	if (!doc["metadata"].IsObject() ||
		str != "itpskel" ||
		ver != 1)
	{
		return false;
	}

	// Bonecount
	int bonecount = doc["bonecount"].GetInt();

	// Bones
	const rapidjson::Value& bones = doc["bones"];
	std::string name;
	int parent;
	Quaternion rot;
	Vector3 trans;
	for (rapidjson::SizeType i = 0; i < bones.Size(); i++)
	{
		if (!bones.IsArray() || bones.Size() != bonecount)
		{
			return false;
		}

		GetStringFromJSON(bones[i], "name", name);
		GetIntFromJSON(bones[i], "parent", parent);

		Bone bone;
		bone.mName = name;
		bone.mParent = parent;

		const rapidjson::Value& bindpose = bones[i]["bindpose"];
		if (!bindpose.IsObject())
		{
			return false;
		}
		GetQuaternionFromJSON(bindpose, "rot", rot);
		GetVectorFromJSON(bindpose, "trans", trans);
		bone.mLocalBindPose.mTranslation = trans;
		bone.mLocalBindPose.mRotation = rot;

		mBones.push_back(bone);
	}
	ComputeGlobalInvBindPose();

	return true;
}