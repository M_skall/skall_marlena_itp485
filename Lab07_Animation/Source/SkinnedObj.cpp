#include "stdafx.h"
#include "RenderObj.h"
#include "Component.h"
#include "game.h"
#include "mesh.h"
#include "Shader.h"
#include "texture.h"
#include "VertexBuffer.h"
#include "SkinnedObj.h"

SkinnedObj::SkinnedObj(Game* pGame, const Mesh* pMesh)
	: RenderObj(pGame, pMesh), mGame(pGame)
{
	for (unsigned int i = 0; i < MAX_SKELETON_BONES; i++)
	{
		(mSkinConstants.c_skinMatrix)[i] = Matrix4::Identity;
	}
	mSkinningBuffer = mGame->GetGraphics()->CreateGraphicsBuffer(&mSkinConstants, sizeof(mSkinConstants), D3D11_BIND_CONSTANT_BUFFER, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);
}

SkinnedObj::~SkinnedObj()
{
	mSkinningBuffer->Release();
}

void SkinnedObj::Draw()
{
	mGame->GetGraphics()->UploadBuffer(mSkinningBuffer, &mSkinConstants, sizeof(mSkinConstants));
	mGame->GetGraphics()->GetDeviceContext()->VSSetConstantBuffers(Graphics::CONSTANT_BUFFER_SKINNING, 1, &mSkinningBuffer);
	RenderObj::Draw();
}