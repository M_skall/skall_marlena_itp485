#pragma once
#include "Component.h"
#include "game.h"

class SkinnedObj;
class Animation;
class Skeleton;

class Character : public Component
{
public:
	Character(RenderObj* pObj);
	~Character() override;
	void LoadProperties(const rapidjson::Value& properties) override;
	void Update(float deltaTime) override;

	bool SetAnim(const std::string& animName);
	void UpdateAnim(float deltaTime);
	
private:
	SkinnedObj* mSkinnedObj;
	Skeleton* mSkeleton;
	std::unordered_map<std::string, const Animation*> mAnims;
	const Animation* mCurrentAnim;
	float mAnimationTime; //(Start this off at 0.0)
};