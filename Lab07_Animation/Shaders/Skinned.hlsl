#include "Constants.hlsl"

struct VIn
{
    float3 position : POSITION0;
    float3 normal : NORMAL0;
	uint4 boneIndex : INDICES0;
	float4 boneWeight : WEIGHT0;
    float2 uv : TEXCOORD0;
};

struct VOut
{
    float4 position : SV_POSITION;
    float4 worldPos : POSITION0;
    float4 normal : NORMAL0;
    float2 uv : TEXCOORD0;
};

float4 skin(float4x4 bones[MAX_SKELETON_BONES], // Stores C * B^-1 per bone
	float4 position,
	float4 boneWeights0,
	float4 boneIndices);

VOut VS(VIn vIn)
{
    VOut output;

	//where to store float4??? to then convert to world to projection
	float4 pos = skin(c_skinMatrix, float4(vIn.position, 1.0), vIn.boneWeight, vIn.boneIndex);
	float4 norm = skin(c_skinMatrix, float4(vIn.normal, 0.0), vIn.boneWeight, vIn.boneIndex);

    // transform input position from model to world space
    output.worldPos = mul(pos, c_modelToWorld);
    // transform position from world to projection space
    output.position = mul(output.worldPos, c_viewProj);

    // transform input normal from model to world space
    output.normal = mul(norm, c_modelToWorld);

    output.uv = vIn.uv;

    return output;
}

float4 PS(VOut pIn) : SV_TARGET
{
    float4 diffuse = DiffuseTexture.Sample(DefaultSampler, pIn.uv);

    // do the lighting
    float3 lightColor = c_ambient;
    float3 n = normalize(pIn.normal.xyz);
    float3 v = normalize(c_cameraPosition - pIn.worldPos);
    for (int i = 0; i < MAX_POINT_LIGHTS; ++i)
    {
        if (c_pointLight[i].isEnabled)
        {
            float3 l = c_pointLight[i].position - pIn.worldPos.xyz;
            float dist = length(l);
            if (dist > 0.0)
            {
                l = l / dist;
                float falloff = smoothstep(c_pointLight[i].outerRadius, c_pointLight[i].innerRadius, dist);
                float3 d = falloff * c_pointLight[i].diffuseColor * max(0.0, dot(l, n));
                lightColor += d;

                float3 r = -reflect(l, n);
                float3 s = falloff * c_pointLight[i].specularColor * pow(max(0.0, dot(r, v)), c_pointLight[i].specularPower);
                lightColor += s;
            }
        }
    }

    float4 finalColor = diffuse * float4(lightColor, 1.0);
    return finalColor;
}

float4 skin(float4x4 bones[MAX_SKELETON_BONES], // Stores C * B^-1 per bone
	float4 position,
	float4 boneWeights0,
	float4 boneIndices)
{
	float4 result = boneWeights0.x * mul(position, bones[boneIndices.x]);
	result = result + boneWeights0.y * mul(position, bones[boneIndices.y]);
	result = result + boneWeights0.z * mul(position, bones[boneIndices.z]);
	result = result + boneWeights0.w * mul(position, bones[boneIndices.w]);
	return result;
}