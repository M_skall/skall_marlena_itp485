﻿#pragma once
#include "engineMath.h"
#include <chrono>
#include <ctime>
#include <string>
#include <unordered_map>
#include <stdio.h>

#define PROFILE_SCOPE(name) \
Profiler::ScopedTimer name##_scope(Profiler::Get()->GetTimer(std::string(#name)));

class Profiler
{
public:
	class Timer 
	{
	public:
		void Start()
		{
			mStart = std::chrono::high_resolution_clock::now();
			/*FILE* file;
			fopen_s(&file, "profiler.json", "w");*/
			Profiler* p = Profiler::Get();
			fflush(p->jFile);
			fprintf(p->jFile, "{\"name\": \"\%s\"\, ", GetName().c_str());
			fprintf(p->jFile, "\"cat\": \"\%s\"\, ", GetName().c_str());
			fprintf(p->jFile, "\"ph\": \"B\", \"ts\": ");
			fprintf(p->jFile, "%llu, ", mStart.time_since_epoch().count() / 1000);
			fprintf(p->jFile, "\"pid\": %d, ", 1);
			fprintf(p->jFile, "\"tid\": %d \n", 1);
			fprintf(p->jFile, "}, \n");
				/*
			{"name": "myFunction", "cat": "foo", "ph": "B", "ts": 123, "pid": 2343, "tid": 2347,
			 "args": {
			   "first": 1
			 }
			},
			{"ph": "E", "ts": 145, "pid": 2343, "tid": 2347,
			 "args": {
			   "first": 4,
			   "second": 2
			 }
			}
			*/
			/*
			▪ Both the Start and Stop (“B” and “E”) events require a timestamp (“ts”). You are
			not writing out the duration of the events here. You are writing the start time
			and end time, and Chrome will figure the duration out.
			▪ To get the actual time from a std::chrono::high_resolution_clock::time_point,
			you can use:
			mStart.time_since_epoch().count()
			▪ And Chrome wants this in microseconds (you have nanoseconds), so divide by
			1000.
			o To be a legal json file, you must not allow the last “E” event to end with a comma, even
			though every line up to that point does need a comma.

			*/
			// write out the “B” event for that timer

		}
		void Stop()
		{
			std::chrono::high_resolution_clock::time_point stop = std::chrono::high_resolution_clock::now();
			double duration = (double)std::chrono::duration_cast<std::chrono::nanoseconds>(stop - mStart).count();
			// 1  nanosecond = 1 x 10-6 milliseconds
			mCurrent_ms += duration / 1000000;
			 

			Profiler* p = Profiler::Get();
			fflush(p->jFile);
			fprintf(p->jFile, "{\"ph\": \"E\", \"ts\": ");
			fprintf(p->jFile, "%llu, ", stop.time_since_epoch().count() / 1000);
			fprintf(p->jFile, "\"pid\": %d, ", 1);
			fprintf(p->jFile, "\"tid\": %d \n", 1);
			fprintf(p->jFile, "},\n");
		}
		void Reset()
		{
			if (mCurrent_ms > 0.0f) {
				mTotalTimes_ms += mCurrent_ms;
				mNumSample++;
			}
			/*mTotalTimes_ms += mCurrent_ms;
			mNumSample++;*/
			//update mMax_ms; if greater than previous maximum, then update
			if (mCurrent_ms > mMax_ms)
			{
				mMax_ms = mCurrent_ms;
			}
			mCurrent_ms = 0;
		}
		const std::string& GetName() const
		{
			return mName;
		}
		double GetTime_ms() const
		{
			return mCurrent_ms;
		}
		double GetMax_ms() const
		{
			return mMax_ms;
		}
		double GetAvg_ms() const
		{
			return mTotalTimes_ms / mNumSample;
		}

	private:
		Timer(std::string name) : mCurrent_ms(0), mMax_ms(0), mTotalTimes_ms(0), mNumSample(0) 
		{
			mName = name;
		}
		~Timer() {}
		
		std::string mName; // (a name to describe what this timer represents)
		double mCurrent_ms; // (how long this timer took in the most recent sample – in milliseconds)
		double mMax_ms; // (how long was this timer in the longest sample – milliseconds)
		double mTotalTimes_ms; // (the total time for this timer for all samples combined)
		int mNumSample; // (how many samples have been taken for this timer)
		std::chrono::high_resolution_clock::time_point mStart; // (record the time when the timer is started.)

		friend class Profiler;
	}; 

	class ScopedTimer
	{
	public:
		ScopedTimer(class Timer* timer)
		{
			mTimer = timer;
			mTimer->Start();
		}
		~ScopedTimer()
		{
			mTimer->Stop();
		}
	private:
		class Timer* mTimer;
	};

	Timer* GetTimer(const std::string &name)
	{
		auto iter = mTimers.find(name);
		if (iter != mTimers.end()) {
			return iter->second;
		}
		else {
			Timer* t = new Timer(name);
			mTimers.insert(std::make_pair(name, t));
			return t;
		}
	}

	// return address of a local static Profiler
	static Profiler* Get()
	{
		static Profiler p;
		return &p;
	}

	// Loop through all the Timers in mTimers and Reset() them.
	void ResetAll()
	{
		for (auto it = mTimers.begin(); it != mTimers.end(); ++it) {
			it->second->Reset();
			/*Timer* t = it->second;
			t->Reset();*/
		}
	}

	FILE* jFile;
private:
	Profiler() 
	{
		//FILE* file;
		fopen_s(&jFile, "profile.json", "w");
		fprintf(jFile, "[ \n");
		//fclose(jFile);
	}
	~Profiler() 
	{
		FILE* file;
		fopen_s(&file, "profile.txt", "w");

		fprintf(file, "name: avg (ms), max (ms) \n");
		for (auto it = mTimers.begin(); it != mTimers.end(); ++it) {
			//fprintf(file, "name:, avg (ms), max (ms) \n");
			fprintf(file, "%s: ", it->first.c_str());
			fprintf(file, "%f, ", it->second->GetAvg_ms());
			fprintf(file, "%f \n", it->second->GetMax_ms());
		}
		fclose(file);

		//FILE* jfile;
		//fopen_s(&jFile, "profile.json", "w");
		fflush(jFile);
		long size = ftell(jFile);
		fseek(jFile, size-3, SEEK_SET);   // non-portable
		fprintf(jFile, "\n]");
		fclose(jFile);
	}
	
	std::unordered_map<std::string, Timer*> mTimers;
};