#pragma once
#include "engineMath.h"
#include "Graphics.h"
#include <array>

class Game;
class Shader;
class VertexBuffer;
class Texture;
class Mesh;
class Component;

class RenderObj
{
public:
    struct PerObjectConstants
    {
        Matrix4 c_modelToWorld;
    };

    RenderObj(Game *pGame, Mesh* pMesh);
    virtual ~RenderObj();
    virtual void Draw();

    PerObjectConstants mObjectData;

	// TODO Lab 05d
	//void SetTexture(int slot, const Texture *texture);

	// TODO Lab 05l
	virtual void Update(float deltaTime);

	// TODO Lab 05l
	Game* GetGame() { return mGame; }
	void AddComponent(Component *pComp);
	Vector3 GetPosition() { return mPos; }
	void SetPosition(Vector3 pos) { mPos = pos; }

protected:
    Game* mGame;

	// TODO Lab 05f
    ID3D11Buffer* mObjectBuffer;
	const Mesh* mMesh;

	// TODO Lab05l
	std::vector<Component*> mComponents;
	Vector3 mPos;
};
