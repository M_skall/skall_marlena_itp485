#include "stdafx.h"
#include "jsonUtil.h"
#include "Game.h"
#include "Camera.h"
#include "engineMath.h"
#include "Graphics.h"
#include "RenderCube.h"
#include "RenderObj.h"
#include "Shader.h"
#include "stringUtil.h"
#include "texture.h"
#include "VertexBuffer.h"
#include "mesh.h"
#include "Component/PointLight.h"
#include "rapidjson\include\rapidjson\rapidjson.h"
#include "rapidjson\include\rapidjson\document.h"
#include <fstream>
#include <sstream>
#include "Profiler.h"

Game::Game()
	: mCacheShader(this),
	mCacheTexture(this),
	mCacheMesh(this)
{
}

Game::~Game()
{
}

void Game::Init(HWND hWnd, float width, float height)
{
	// TODO Lab 02b
    mGraphics.InitD3D(hWnd, width, height);
    {
        // TODO Lab 02e
        VertexPosColor vert[] =
        {
            { Vector3(0.0f,   0.5f, 0.0f), Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f) },
            { Vector3(0.45f, -0.5,  0.0f), Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f) },
            { Vector3(-0.45f, -0.5f, 0.0f), Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f) }
        };
        uint16_t index[] = { 0, 1, 2 };

        // TODO Lab 02f
        D3D11_INPUT_ELEMENT_DESC inputElem[] =
        {
            { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosColor, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(VertexPosColor, color), D3D11_INPUT_PER_VERTEX_DATA, 0}
        };
		mCacheShader.Cache(L"Mesh", new Shader(&mGraphics));
        //mSimpleShader = new Shader(&mGraphics);
		GetShader(L"Mesh")->Load(L"Shaders/Mesh.hlsl", inputElem, sizeof(inputElem) / sizeof(inputElem[0]));

        VertexBuffer* pVertBuff = new VertexBuffer(&mGraphics,
            vert, sizeof(vert) / sizeof(vert[0]), sizeof(vert[0]),
            index, sizeof(index) / sizeof(index[0]), sizeof(index[0])
            );

		mCacheMesh.Cache(L"Mesh", new Mesh(this, pVertBuff, GetShader(L"Mesh")));
        //RenderObj* triangle = new RenderObj(this, new Mesh(this, pVertBuff, GetShader(L"Mesh")));
		//mRenderObj.push_back(triangle);
    }
    {
        D3D11_INPUT_ELEMENT_DESC inputElem[] =
        {
            { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNormColorUV, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNormColorUV, norm), D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(VertexPosNormColorUV, color), D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(VertexPosNormColorUV, uv), D3D11_INPUT_PER_VERTEX_DATA, 0 }
        };
		mCacheShader.Cache(L"BasicMesh", new Shader(&mGraphics));
        //mBasicMeshShader = new Shader(&mGraphics);
		GetShader(L"BasicMesh")->Load(L"Shaders/BasicMesh.hlsl", inputElem, sizeof(inputElem) / sizeof(inputElem[0]));

		D3D11_INPUT_ELEMENT_DESC inputElem2[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNormUV, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNormUV, norm), D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(VertexPosNormUV, uv), D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};
		// TODO Lab 05h
		mCacheShader.Cache(L"Phong", new Shader(&mGraphics));
		GetShader(L"Phong")->Load(L"Shaders/Phong.hlsl", inputElem2, sizeof(inputElem2) / sizeof(inputElem2[0]));
		mCacheShader.Cache(L"Unlit", new Shader(&mGraphics));
		GetShader(L"Unlit")->Load(L"Shaders/Unlit.hlsl", inputElem2, sizeof(inputElem2) / sizeof(inputElem2[0]));
    }
	LoadTexture(L"Assets/Textures/Cube.png");
    //mTexture = new Texture(&mGraphics);
    //mTexture->Load(L"Assets/Textures/Cube.png");

	//RenderObj* cube = new RenderObj(this, LoadMesh(L"Assets/Meshes/PlayerShip.itpmesh2"));
	//cube->SetTexture(Graphics::TEXTURE_SLOT_DIFFUSE, LoadTexture(L"Assets/Textures/Cube.png"));
	//mRenderObj.push_back(cube);

	mCamera = new Camera(&mGraphics);
	LoadLevel(L"Assets/Levels/Level05.itplevel");

    mLightingBuffer = mGraphics.CreateGraphicsBuffer(&mLightData, sizeof(mLightData), D3D11_BIND_CONSTANT_BUFFER, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);
   /* SetAmbientLight(Vector3(0.1f, 0.1f, 0.1f));
    {
        PointLightData* pLight1 = AllocateLight();
        pLight1->diffuseColor = Vector3(1.0f, 1.0f, 1.0f);
        pLight1->specularColor = Vector3(1.0f, 1.0f, 1.0f);
        pLight1->position = Vector3(-100.0f, 0.0f, 50.0f);
        pLight1->specularPower = 10.0f;
        pLight1->innerRadius = 20.0f;
        pLight1->outerRadius = 200.0f;
    }
    {
        PointLightData* pLight2 = AllocateLight();
        pLight2->diffuseColor = Vector3(1.0f, 1.0f, 1.0f);
        pLight2->specularColor = Vector3(1.0f, 1.0f, 1.0f);
        pLight2->position = Vector3(75.0f, 150.0f, 100.0f);
        pLight2->specularPower = 10.0f;
        pLight2->innerRadius = 20.0f;
        pLight2->outerRadius = 200.0f;
    }*/
}

void Game::Shutdown()
{
    mGraphics.CleanD3D();
	mCacheShader.Clear();
	mCacheTexture.Clear();
	mCacheMesh.Clear();

	// TODO Lab 05e
	for (auto i : mRenderObj) {
		delete i;
	}

    delete mCamera;
    mLightingBuffer->Release();
}

static float s_angle = 0.0f;
void Game::Update(float deltaTime)
{
    s_angle += Math::Pi * deltaTime;
	/*mRenderObj[0]->mObjectData.c_modelToWorld = Matrix4::CreateScale(300.0f) * Matrix4::CreateRotationZ(s_angle);

	mRenderObj[1]->mObjectData.c_modelToWorld = Matrix4::CreateScale(2.0f)
        * Matrix4::CreateRotationY(s_angle)
        * Matrix4::CreateRotationX(0.25f*s_angle);*/


	// TODO Lab 05l
	// loop through all the RenderObj in mRenderObj and call Update() on them
	for (unsigned int i = 0; i < mRenderObj.size(); i++)
	{
		mRenderObj[i]->Update(deltaTime);
	}
}

void Game::RenderFrame()
{
	// TODO Lab 02b
	Graphics::Color4 clearColor(0.0f, 0.2f, 0.4f, 1.0f);

	{
		PROFILE_SCOPE(BeginFrame);
		mGraphics.BeginFrame(clearColor);
	}

	mCamera->SetActive();
	{
		PROFILE_SCOPE(UploadBuffer);
		mGraphics.UploadBuffer(mLightingBuffer, &mLightData, sizeof(mLightData));
	}
	{
		PROFILE_SCOPE(SetConstantBuffers);
		mGraphics.GetDeviceContext()->PSSetConstantBuffers(Graphics::CONSTANT_BUFFER_LIGHTING, 1, &mLightingBuffer);
	}
	LoadTexture(L"Assets/Textures/Cube.png")->SetActive(Graphics::TEXTURE_SLOT_DIFFUSE);

	// TODO La b05e

	for (auto i : mRenderObj) {
		i->Draw();
	}
	{
		PROFILE_SCOPE(EndFrame);
		mGraphics.EndFrame();
	}
}

void Game::OnKeyDown(uint32_t key)
{
	m_keyIsHeld[key] = true;
}

void Game::OnKeyUp(uint32_t key)
{
	m_keyIsHeld[key] = false;
}

bool Game::IsKeyHeld(uint32_t key) const
{
	const auto find = m_keyIsHeld.find(key);
	if (find != m_keyIsHeld.end())
		return find->second;
	return false;
}

Game::PointLightData* Game::AllocateLight()
{
    for (int i = 0; i < MAX_POINT_LIGHTS; ++i)
    {
        if (false == mLightData.c_pointLight[i].isEnabled)
        {
            mLightData.c_pointLight[i].isEnabled = true;
            return &mLightData.c_pointLight[i];
        }
    }
    return nullptr;
}

void Game::FreeLight(PointLightData* pLight)
{
    pLight->isEnabled = false;
}

bool Game::LoadLevel(const WCHAR* fileName)
{
	std::ifstream file(fileName);
	if (!file.is_open())
	{
		return false;
	}

	std::stringstream fileStream;
	fileStream << file.rdbuf();
	std::string contents = fileStream.str();
	rapidjson::StringStream jsonStr(contents.c_str());
	rapidjson::Document doc;
	doc.ParseStream(jsonStr);

	if (!doc.IsObject())
	{
		return false;
	}

	std::string str = doc["metadata"]["type"].GetString();
	int ver = doc["metadata"]["version"].GetInt();

	// Check the metadata
	if (!doc["metadata"].IsObject() ||
		str != "itplevel" ||
		ver != 2)
	{
		return false;
	}

	// TODO Lab 05j
	// load the "camera" data
	const rapidjson::Value& camera = doc["camera"];
	Vector3 pos;
	Quaternion rot;
	GetVectorFromJSON(camera, "position", pos);
	GetQuaternionFromJSON(camera, "rotation", rot);
	Matrix4 view = mCamera->GetView();
	view = Matrix4::CreateFromQuaternion(rot) * Matrix4::CreateTranslation(pos);
	view.Invert();
	mCamera->SetView(view);

	// Lighting Data
	const rapidjson::Value& lighting = doc["lightingData"];
	Vector3 light;
	GetVectorFromJSON(lighting, "ambient", light);
	mLightData.c_ambient = light;

	// Render Objects
	const rapidjson::Value& renderObjs = doc["renderObjects"];
	std::wstring meshName;
	std::string compType;
	Vector3 renderPos;
	Quaternion renderRot;
	float scale;

	for (rapidjson::SizeType i = 0; i < renderObjs.Size(); i++)
	{
		if (!renderObjs[i].IsObject())
		{
			return false;
		}

		GetVectorFromJSON(renderObjs[i], "position", renderPos);
		GetQuaternionFromJSON(renderObjs[i], "rotation", renderRot);
		GetFloatFromJSON(renderObjs[i], "scale", scale);
		GetWStringFromJSON(renderObjs[i], "mesh", meshName);

		RenderObj* renderObj = new RenderObj(this, LoadMesh(meshName));
		renderObj->mObjectData.c_modelToWorld = Matrix4::Identity;
		renderObj->mObjectData.c_modelToWorld = Matrix4::CreateScale(scale) * Matrix4::CreateTranslation(renderPos) * Matrix4::CreateFromQuaternion(renderRot);
		mRenderObj.push_back(renderObj);

		renderObj->SetPosition(renderPos);

		std::string type;
		const rapidjson::Value& components = renderObjs[i]["components"];
		for (rapidjson::SizeType i = 0; i < components.Size(); i++)
		{
			GetStringFromJSON(components[i], "type", type);
			if (type == "PointLight") {
				PointLight* pointLight = new PointLight(renderObj);
				pointLight->LoadProperties(components[i]);
				renderObj->AddComponent(pointLight);
			}
		}
	}

	return true;
}

Shader* Game::GetShader(const std::wstring& shaderName)
{
	Shader* shader = mCacheShader.Get(shaderName);
	return shader;
}

Texture* Game::LoadTexture(const std::wstring& fileName)
{
	Texture* texture = mCacheTexture.Load(fileName);
	return texture;
}

Mesh* Game::LoadMesh(const std::wstring& fileName)
{
	Mesh* mesh = mCacheMesh.Load(fileName);
	return mesh;
}