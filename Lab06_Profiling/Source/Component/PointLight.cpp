#include "stdafx.h"
#include "jsonUtil.h"
#include "RenderObj.h"
#include "Graphics.h"
#include "Component/PointLight.h"
#include "stringUtil.h"

PointLight::PointLight(RenderObj* pObj)
	: Component(pObj)
{
	mLight = pObj->GetGame()->AllocateLight();
	mGame = pObj->GetGame();
	mLight->position = pObj->GetPosition();
}

PointLight::~PointLight()
{
	mGame->FreeLight(mLight);
}

void PointLight::LoadProperties(const rapidjson::Value& properties)
{
	std::string type;
	Vector3 diffuseColor;
	Vector3 specularColor;
	float specularPower;
	float innerRadius;
	float outerRadius;

	//for (rapidjson::SizeType i = 0; i < properties.Size(); i++)
	//{
		GetStringFromJSON(properties, "type", type);
		GetVectorFromJSON(properties, "diffuseColor", diffuseColor);
		GetVectorFromJSON(properties, "specularColor", specularColor);
		GetFloatFromJSON(properties, "specularPower", specularPower);
		GetFloatFromJSON(properties, "innerRadius", innerRadius);
		GetFloatFromJSON(properties, "outerRadius", outerRadius);
		

		mLight->diffuseColor = diffuseColor;
		mLight->specularColor = specularColor;
		mLight->specularPower = specularPower;
		mLight->innerRadius = innerRadius;
		mLight->outerRadius = outerRadius;

	//}
}