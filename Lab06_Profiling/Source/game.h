#pragma once
#include "Graphics.h"
#include "engineMath.h"
#include "assetCache.h"

class Camera;
class RenderObj;
class Shader;
class Texture;
class Mesh;

#define MAX_POINT_LIGHTS 8


class Game
{
public:
    struct PointLightData
    {
        Vector3 diffuseColor;
        float pad0;
        Vector3 specularColor;
        float pad1;
        Vector3 position;
        float specularPower;
        float innerRadius;
        float outerRadius;
        bool isEnabled;
        float pad2;
    };

    struct LightingData
    {
        Vector3	c_ambient;
        float pad;
        PointLightData c_pointLight[MAX_POINT_LIGHTS];
    };

    Game();
    ~Game();

    void Init(HWND hWnd, float width, float height);
    void Shutdown();
	void Update(float deltaTime);
    void RenderFrame();

	void OnKeyDown(uint32_t key);
	void OnKeyUp(uint32_t key);
	bool IsKeyHeld(uint32_t key) const;

	// TODO Lab 02b
    Graphics* GetGraphics() { return &mGraphics; }

	// TODO Lab04
    PointLightData* AllocateLight();
    void FreeLight(PointLightData* pLight);
    void SetAmbientLight(const Vector3& color) { mLightData.c_ambient = color; }
    const Vector3 &GetAmbientLight() const { return mLightData.c_ambient; }

	Shader* GetShader(const std::wstring& shaderName);
	Texture* LoadTexture(const std::wstring& fileName);
	Mesh* LoadMesh(const std::wstring& fileName);

private:
	std::unordered_map<uint32_t, bool> m_keyIsHeld;
	// TODO Lab 02
    Graphics mGraphics;
    Camera* mCamera;
    LightingData mLightData;
    ID3D11Buffer *mLightingBuffer;

	// TODO Lab05a
	AssetCache<Shader> mCacheShader;
	AssetCache<Texture> mCacheTexture;
	AssetCache<Mesh> mCacheMesh;

	// TODO Lab05e
	std::vector<RenderObj*> mRenderObj;

	bool LoadLevel(const WCHAR* fileName);
};

struct VertexPosColor
{
    Vector3 pos;
    Graphics::Color4 color;
};

struct VertexPosColorUV
{
    Vector3 pos;
    Graphics::Color4 color;
    Vector2 uv;
};

struct VertexPosNormColorUV
{
    Vector3 pos;
    Vector3 norm;
    Graphics::Color4 color;
    Vector2 uv;
};

struct VertexPosNormUV
{
	Vector3 pos;
	Vector3 norm;
	Vector2 uv;
};