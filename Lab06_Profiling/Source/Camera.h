#pragma once
#include "engineMath.h"

class Graphics;

class Camera
{
public:
    struct PerCameraConstants
    {
        Matrix4 c_viewProj;
        Vector3 c_cameraPosition;
        float pad;
    };

    Camera(Graphics* pGraphics);
    ~Camera();
    void SetActive();

	Matrix4 GetView() { return mView; }
	void SetView(Matrix4 view) { mView = view; }
	Matrix4 GetProj() { return mView; }
	void SetProj(Matrix4 proj) { mProj = proj; }

    PerCameraConstants mCameraData;

private:
    Graphics* mGraphics;
    Matrix4 mView;
    Matrix4 mProj;
    ID3D11Buffer* mCameraBuffer;
};