#include "stdafx.h"
#include "RenderObj.h"
#include "game.h"
#include "Shader.h"
#include "VertexBuffer.h"

RenderObj::RenderObj(Game *pGame,
    const VertexBuffer *vertexBuffer, const Shader *shader)
    : mGame(pGame)
    , mVertexBuffer(vertexBuffer)
    , mShader(shader)
{
    mObjectData.c_modelToWorld = Matrix4::Identity;
    mObjectData.c_modelToWorld = Matrix4::CreateRotationZ(Math::ToRadians(45.0f));
    mObjectBuffer = mGame->GetGraphics()->CreateGraphicsBuffer(&mObjectData, sizeof(mObjectData), D3D11_BIND_CONSTANT_BUFFER, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);
}

RenderObj::~RenderObj()
{
    delete mVertexBuffer;
    mObjectBuffer->Release();
}

void RenderObj::Draw()
{
    mShader->SetActive();
    mGame->GetGraphics()->UploadBuffer(mObjectBuffer, &mObjectData, sizeof(mObjectData));
    mGame->GetGraphics()->GetDeviceContext()->VSSetConstantBuffers(Graphics::CONSTANT_BUFFER_RENDEROBJ, 1, &mObjectBuffer);
    mVertexBuffer->SetActive();
    mVertexBuffer->Draw();
}
