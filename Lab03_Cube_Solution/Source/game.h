#pragma once
#include "Graphics.h"
#include "engineMath.h"

class Camera;
class Shader;
class RenderObj;

class Game
{
public:
    Game();
    ~Game();

    void Init(HWND hWnd, float width, float height);
    void Shutdown();
	void Update(float deltaTime);
    void RenderFrame();

	void OnKeyDown(uint32_t key);
	void OnKeyUp(uint32_t key);
	bool IsKeyHeld(uint32_t key) const;

	// TODO Lab 02b
    Graphics* GetGraphics() { return &mGraphics; }

private:
	std::unordered_map<uint32_t, bool> m_keyIsHeld;
	// TODO Lab 02b
    Graphics mGraphics;
	// TODO Lab 02e
	// TODO Lab 02f
    Shader* mSimpleShader;

    RenderObj* mTriangle;
    RenderObj* mCube;
    Camera* mCamera;

	bool LoadLevel(const WCHAR* fileName);
};

struct VertexPosColor
{
    Vector3 pos;
    Graphics::Color4 color;
};
