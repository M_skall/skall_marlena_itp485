#include "stdafx.h"
#include "Graphics.h"
#include "Shader.h"
#include "engineMath.h"

#pragma comment (lib, "d3d11.lib") 

Graphics::Graphics()
    : mScreenWidth(0)
    , mScreenHeight(0)
{
}

Graphics::~Graphics()
{
}

void Graphics::InitD3D(HWND hWnd, float width, float height)
{
    mScreenWidth = width;
    mScreenHeight = height;

	// TODO Lab 02c
    // clear out the struct for use
    DXGI_SWAP_CHAIN_DESC scd;
    ZeroMemory(&scd, sizeof(DXGI_SWAP_CHAIN_DESC));
    // fill the swap chain description struct
    scd.BufferCount = 1;                                 // one back buffer
    scd.BufferDesc.Width = (UINT)mScreenWidth;
    scd.BufferDesc.Height = (UINT)mScreenHeight;
    scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;  // use 32-bit color
    scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;   // how swap chain is to be used
    scd.OutputWindow = hWnd;                             // the window to be used
    scd.SampleDesc.Count = 1;                            // how many multisamples
    scd.Windowed = TRUE;                                 // windowed/full-screen mode
    HRESULT hr = D3D11CreateDeviceAndSwapChain(NULL,
        D3D_DRIVER_TYPE_HARDWARE,
        NULL,
        D3D11_CREATE_DEVICE_DEBUG,
        NULL,
        NULL,
        D3D11_SDK_VERSION,
        &scd,
        &mSwapchain,
        &mDev,
        NULL,
        &mDevcon);
    D3D11_VIEWPORT viewport;
    ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));
    viewport.Width = width;
    viewport.Height = height;
    viewport.MaxDepth = 1.0f;
    viewport.MinDepth = 0.0f;
    mDevcon->RSSetViewports(1, &viewport);

	// TODO Lab 02d
    ID3D11Texture2D *pBackBuffer;
    hr = mSwapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
    DbgAssert(hr == S_OK, "Something wrong with your back buffer");
    mDev->CreateRenderTargetView(pBackBuffer, nullptr, &mBackbuffer);
    pBackBuffer->Release();
    mDevcon->OMSetRenderTargets(1, &mBackbuffer, nullptr);
    mDevcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void Graphics::BeginFrame(const Color4 &clearColor)
{
	// TODO Lab 02d
    mDevcon->ClearRenderTargetView(mBackbuffer, (float*)&clearColor);
}

void Graphics::EndFrame()
{
	// TODO Lab 02c
    mSwapchain->Present(1, 0);
}

void Graphics::CleanD3D()
{
	// TODO Lab 02c
    mSwapchain->Release();
    mDev->Release();
    mDevcon->Release();
	// TODO Lab 02d
    mBackbuffer->Release();
}

ID3D11Buffer* Graphics::CreateGraphicsBuffer(const void *initialData, int inDataSize, D3D11_BIND_FLAG inBindFlags, D3D11_CPU_ACCESS_FLAG inCPUAccessFlags, D3D11_USAGE inUsage)
{
    D3D11_BUFFER_DESC desc;
    ZeroMemory(&desc, sizeof(desc));
    desc.Usage = inUsage;
    desc.ByteWidth = inDataSize;
    desc.BindFlags = inBindFlags;
    desc.CPUAccessFlags = inCPUAccessFlags;
    ID3D11Buffer* pBuffer;
    GetDevice()->CreateBuffer(&desc, nullptr, &pBuffer);
    if (nullptr != initialData)
        UploadBuffer(pBuffer, initialData, inDataSize);
    return pBuffer;
}

void Graphics::UploadBuffer(ID3D11Buffer* buffer, const void* data, size_t dataSize)
{
    D3D11_MAPPED_SUBRESOURCE map;
    GetDeviceContext()->Map(buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &map);
    memcpy(map.pData, data, dataSize);
    GetDeviceContext()->Unmap(buffer, 0);
}
