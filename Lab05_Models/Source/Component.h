#pragma once

#include "engineMath.h"
#include "jsonUtil.h"

class Camera;
class RenderObj;
class Shader;
class Texture;
class Mesh;

class Component
{
public:
	Component(RenderObj *pObj);
	virtual ~Component();
	virtual void LoadProperties(const rapidjson::Value& properties);
	virtual void Update(float deltaTime);

private:
	RenderObj* mObj;
};
