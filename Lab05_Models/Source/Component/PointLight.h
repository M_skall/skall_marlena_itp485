#pragma once

#include "Component.h"
#include "engineMath.h"
#include <fstream>
#include <sstream>
#include "rapidjson\include\rapidjson\rapidjson.h"
#include "rapidjson\include\rapidjson\document.h"
#include "game.h"

class RenderObj;

class PointLight : public Component
{
public:
	PointLight(RenderObj *pObj);
	~PointLight();
	void LoadProperties(const rapidjson::Value& properties) override;

private:
	Game::PointLightData* mLight;
	Game* mGame;
};
