#include "stdafx.h"
#include "RenderObj.h"
#include "game.h"
#include "Shader.h"
#include "VertexBuffer.h"
#include "texture.h"
#include "mesh.h"
#include "Component.h"

RenderObj::RenderObj(Game *pGame, Mesh* pMesh)
    : mGame(pGame), mMesh(pMesh)
{
    mObjectData.c_modelToWorld = Matrix4::Identity;
    mObjectData.c_modelToWorld = Matrix4::CreateRotationZ(Math::ToDegrees(45.0f));
    mObjectBuffer = mGame->GetGraphics()->CreateGraphicsBuffer(&mObjectData, sizeof(mObjectData), D3D11_BIND_CONSTANT_BUFFER, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);
}

RenderObj::~RenderObj()
{
    mObjectBuffer->Release();
}

void RenderObj::Draw()
{
    mGame->GetGraphics()->UploadBuffer(mObjectBuffer, &mObjectData, sizeof(mObjectData));
    mGame->GetGraphics()->GetDeviceContext()->VSSetConstantBuffers(Graphics::CONSTANT_BUFFER_RENDEROBJ, 1, &mObjectBuffer);
	mMesh->Draw();
}

//void RenderObj::SetTexture(int slot, const Texture *texture)
//{
//	mMesh->SetTexture(slot, texture);
//}

// TODO Lab 05l
// Loop through all the Components and call Update() on each of them.
void RenderObj::Update(float deltaTime)
{
	for (unsigned int i = 0; i < mComponents.size(); i++) {
		mComponents[i]->Update(deltaTime);
	}
}


void RenderObj::AddComponent(Component *pComp)
{
	mComponents.push_back(pComp);
}