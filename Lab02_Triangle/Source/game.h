#pragma once
#include "Graphics.h"


class Game
{
public:
    Game();
    ~Game();

    void Init(HWND hWnd, float width, float height);
    void Shutdown();
	void Update(float deltaTime);
    void RenderFrame();

	void OnKeyDown(uint32_t key);
	void OnKeyUp(uint32_t key);
	bool IsKeyHeld(uint32_t key) const;

	// TODO Lab 02b
	class Graphics* GetGraphics() { return &mGraphics; }

private:
	std::unordered_map<uint32_t, bool> m_keyIsHeld;
	// TODO Lab 02b
	class Graphics mGraphics;

	// TODO Lab 02e
	ID3D11Buffer* mTriangleVert;

	// TODO Lab 02f
	class Shader* mSimpleShader;

	UINT vertsize;

	bool LoadLevel(const WCHAR* fileName);
};