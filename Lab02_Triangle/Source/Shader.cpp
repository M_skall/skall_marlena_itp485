﻿#include "stdafx.h"
#include "Shader.h"
#include "Graphics.h"
#include <d3dcompiler.h>

#pragma comment (lib, "d3dcompiler.lib") 

Shader::Shader(Graphics* graphics)
    : mGraphics(graphics)
{
}

Shader::~Shader()
{
	vs->Release();
	ps->Release();
	inputlayout->Release();
}

static bool LoadShader(const WCHAR* filename, const char* entryPoint, const char* model, ID3DBlob*& pBlobOut)
{
    HRESULT hr = S_OK;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#ifdef _DEBUG
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;

    // Disable optimizations to further improve shader debugging
    dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

    ID3DBlob* pErrorBlob = nullptr;
    hr = D3DCompileFromFile(filename, nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, entryPoint, model,
        dwShaderFlags, 0, &pBlobOut, &pErrorBlob);
    if (FAILED(hr))
    {
        if (pErrorBlob)
        {
            static wchar_t szBuffer[4096];
            _snwprintf_s(szBuffer, 4096, _TRUNCATE,
                L"%hs",
                (char*)pErrorBlob->GetBufferPointer());
            OutputDebugString(szBuffer);
#ifdef _WINDOWS
            MessageBox(nullptr, szBuffer, L"Error", MB_OK);
#endif
            pErrorBlob->Release();
        }
        return false;
    }
    if (pErrorBlob)
    {
        pErrorBlob->Release();
    }

    return true;
}

bool Shader::Load(const WCHAR* fileName, const D3D11_INPUT_ELEMENT_DESC* layoutArray, int numLayoutElements)
{
	// TODO Lab 02g
	/*
	The vertex shader entry point is “VS”. This is whatever you’ve named the vertex
	shader function in your HLSL.
	▪ The vertex shader model is “vs_4_0”. Stands for vertex shader model 4.0.
	▪ The pixel shader entry point is “PS”. This is whatever you’ve named the pixel
	shader function in your HLSL.
	▪ The pixel shader model is “ps_4_0”. Stands for pixel shader model 4.0.
	▪ Each call to LoadShader() also needs a ID3DBlob*. You can create these on the
	stack
	LoadShader(const WCHAR* filename, const char* entryPoint, const char* model, ID3DBlob*& pBlobOut)
	*/
	const char* entryPointVS = "VS";
	const char* entryPointPS = "PS";
	ID3DBlob* blobVS;
	ID3DBlob* blobPS;
	LoadShader(fileName, entryPointVS, "vs_4_0", blobVS);
	LoadShader(fileName, entryPointPS, "ps_4_0", blobPS);
	mGraphics->GetDevice()->CreateVertexShader(blobVS->GetBufferPointer(), blobVS->GetBufferSize(), NULL, &vs);
	mGraphics->GetDevice()->CreatePixelShader(blobPS->GetBufferPointer(), blobPS->GetBufferSize(), NULL, &ps);
	mGraphics->GetDevice()->CreateInputLayout(layoutArray, numLayoutElements, blobVS->GetBufferPointer(), blobVS->GetBufferSize(), &inputlayout);

	blobVS->Release();
	blobPS->Release();

	return false;
}

void Shader::SetActive() const
{
	// TODO Lab 02g
	mGraphics->GetDeviceContext()->VSSetShader(vs, NULL, NULL);
	mGraphics->GetDeviceContext()->PSSetShader(ps, NULL, NULL);
	mGraphics->GetDeviceContext()->IASetInputLayout(inputlayout);
}