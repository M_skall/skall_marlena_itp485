﻿#include "stdafx.h"
#include "Game.h"
#include "engineMath.h"
#include "Graphics.h"
#include "Shader.h"
#include "stringUtil.h"
#include "rapidjson\include\rapidjson\rapidjson.h"
#include "rapidjson\include\rapidjson\document.h"
#include <fstream>
#include <sstream>


// TODO Lab 02e
struct VertexPosColor
{
	Vector3 pos;
	Graphics::Color4 color;
};

Game::Game()
{
}

Game::~Game()
{
}

void Game::Init(HWND hWnd, float width, float height)
{
	// TODO Lab 02b
	mGraphics.InitD3D(hWnd, width, height);

	// TODO Lab 02e
	//vertex buffer with triangle data in it
	VertexPosColor vert[] =
	{
		{ Vector3(0.0f, 0.5f, 0.0f), Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f) },
		{ Vector3(0.45f, -0.5, 0.0f), Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f) },
		{ Vector3(-0.45f, -0.5f, 0.0f), Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f) }
	};
	
	vertsize = sizeof(vert[0]);
	D3D11_BUFFER_DESC bufferdesc;
	ZeroMemory(&bufferdesc, sizeof(D3D11_BUFFER_DESC));
	/*
	▪ Usage: D3D11_USAGE_DYNAMIC
	▪ ByteWidth: How many bytes for the entire buffer: sizeof(vert)
	▪ BindFlags: D3D11_BIND_VERTEX_BUFFER
	▪ CPUAccessFlags: D3D11_CPU_ACCESS_WRITE
	*/
	bufferdesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferdesc.ByteWidth = sizeof(vert);
	bufferdesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferdesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	mGraphics.GetDevice()->CreateBuffer(&bufferdesc, NULL, &mTriangleVert);
	
	D3D11_MAPPED_SUBRESOURCE subresource;
	/* copy the triangle vertices into your vertex buffer by doing the following:
	▪ “map” the buffer with ID3D11DeviceContext::Map()
		▪ Use a local variable for your D3D11_MAPPED_SUBRESOURCE.
		▪ For “MapType”, you can use D3D11_MAP_WRITE_DISCARD.
	o Use memcpy() to copy the vertex data into your D3D11_MAPPED_SUBRESOURCE::pData.
	o “unmap” the buffer with ID3D11DeviceContext::Unmap()
	*/
	mGraphics.GetDeviceContext()->Map(mTriangleVert, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &subresource);
	//memcpy(&person_copy, &person, sizeof(person));
	memcpy(subresource.pData, vert, sizeof(vert));
	mGraphics.GetDeviceContext()->Unmap(mTriangleVert, 0);

	// TODO Lab 02f
	//load shader
	mSimpleShader = new Shader(GetGraphics());
	D3D11_INPUT_ELEMENT_DESC inputElem[] =
	{
	 { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosColor, pos),
	D3D11_INPUT_PER_VERTEX_DATA, 0 },
	 { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(VertexPosColor, color),
	D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	const wchar_t* fileName = L"Shaders/Simple.hlsl";
	mSimpleShader->Load(fileName, inputElem, 2);

}

void Game::Shutdown()
{
	//delete mSimpleShader;

	// TODO Lab 02b
	//mGraphics.CleanD3D();

	// TODO Lab 02e
	mTriangleVert->Release();

	//mGraphics.CleanD3D();

	// TODO Lab 02f
	delete mSimpleShader;

	mGraphics.CleanD3D();
}

void Game::Update(float deltaTime)
{
}

void Game::RenderFrame()
{
	// TODO Lab 02b
	mGraphics.BeginFrame(Graphics::Color4(0.0f, 0.2f, 0.4f, 1.0f));

	// TODO Lab 02g
	mSimpleShader->SetActive();
	UINT offset = 0;
	mGraphics.GetDeviceContext()->IASetVertexBuffers(0, 1, &mTriangleVert, &vertsize, &offset);
	mGraphics.GetDeviceContext()->Draw(3, 0);

	mGraphics.EndFrame();

}

void Game::OnKeyDown(uint32_t key)
{
	m_keyIsHeld[key] = true;
}

void Game::OnKeyUp(uint32_t key)
{
	m_keyIsHeld[key] = false;
}

bool Game::IsKeyHeld(uint32_t key) const
{
	const auto find = m_keyIsHeld.find(key);
	if (find != m_keyIsHeld.end())
		return find->second;
	return false;
}

bool Game::LoadLevel(const WCHAR* fileName)
{
	std::ifstream file(fileName);
	if (!file.is_open())
	{
		return false;
	}

	std::stringstream fileStream;
	fileStream << file.rdbuf();
	std::string contents = fileStream.str();
	rapidjson::StringStream jsonStr(contents.c_str());
	rapidjson::Document doc;
	doc.ParseStream(jsonStr);

	if (!doc.IsObject())
	{
		return false;
	}

	std::string str = doc["metadata"]["type"].GetString();
	int ver = doc["metadata"]["version"].GetInt();

	// Check the metadata
	if (!doc["metadata"].IsObject() ||
		str != "itplevel" ||
		ver != 2)
	{
		return false;
	}

	return true;
}