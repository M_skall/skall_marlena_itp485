#include "stdafx.h"
#include "Graphics.h"
#include "Shader.h"
#include "engineMath.h"

#pragma comment (lib, "d3d11.lib") 

Graphics::Graphics()
    : mScreenWidth(0)
    , mScreenHeight(0)
{
}

Graphics::~Graphics()
{
}

void Graphics::InitD3D(HWND hWnd, float width, float height)
{
    mScreenWidth = width;
    mScreenHeight = height;

	// TODO Lab 02c
	DXGI_SWAP_CHAIN_DESC scd;
	// clear out the struct for use
	ZeroMemory(&scd, sizeof(DXGI_SWAP_CHAIN_DESC));
	// fill the swap chain description struct
	scd.BufferCount = 1; // one back buffer
	scd.BufferDesc.Width = (UINT)mScreenWidth;
	scd.BufferDesc.Height = (UINT)mScreenHeight;
	scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; // use 32-bit color
	scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT; // how swap chain is to be used
	scd.OutputWindow = hWnd; // the window to be used
	scd.SampleDesc.Count = 1; // how many multisamples
	scd.Windowed = TRUE; // windowed/full-screen mode

	HRESULT hr = D3D11CreateDeviceAndSwapChain(NULL,
			D3D_DRIVER_TYPE_HARDWARE,
			NULL,
			D3D11_CREATE_DEVICE_DEBUG,
			NULL,
			NULL,
			D3D11_SDK_VERSION,
			&scd,
			&mSwapchain,
			&mDev,
			NULL,
			&mDevcon);

	//create viewport
	D3D11_VIEWPORT viewport;
	ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));
	//o Set the corners at 0, 0 and the width and height based on the function arguments.
	//o Set MaxDepth to 1.0 and MinDepth to 0.0.
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;
	viewport.Height = mScreenHeight;
	viewport.Width = mScreenWidth;
	viewport.MaxDepth = 1.0;
	viewport.MinDepth = 0.0;
	mDevcon->RSSetViewports(1, &viewport);

	// TODO Lab 02d
	ID3D11Texture2D *pBackBuffer;
	hr = mSwapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
	DbgAssert(hr == S_OK, "Something wrong with your back buffer");

	mDev->CreateRenderTargetView(pBackBuffer, NULL, &mBackbuffer);
	pBackBuffer->Release();

	mDevcon->OMSetRenderTargets(1, &mBackbuffer, NULL);
	mDevcon->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void Graphics::BeginFrame(const Color4 &clearColor)
{
	// TODO Lab 02d
	//pass in clear color (need to cast to float from Color4)
	const FLOAT* ColorRGBA = &clearColor.r;
	mDevcon->ClearRenderTargetView(mBackbuffer, ColorRGBA);
}

void Graphics::EndFrame()
{
	// TODO Lab 02c
	//Present(UINT SyncInterval, UINT Flags);
	// o Give 1 as the �SyncInterval�(first argument) meaning wait for (at least) 1 vertical blank before swapping buffers.
	// o Give 0 as the �Flags�(second argument).
	mSwapchain->Present(1, 0);
}

void Graphics::CleanD3D()
{
	
	mBackbuffer->Release();

	ID3D11Debug* debug = nullptr;
	HRESULT result = mDev->QueryInterface<ID3D11Debug>(&debug);

	// TODO Lab 02c
	mDevcon->Release();
	mSwapchain->Release();
	mDev->Release();

	// TODO Lab 02d
	//mBackbuffer->Release();

-	debug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
}
