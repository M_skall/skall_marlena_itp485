#include "Constants.hlsl"

#define HEIGHT 150
#define WIDTH 200

struct VIn
{
	float3 position : POSITION0;
	float2 uv : TEXCOORD0;
};

struct VOut
{
	float4 position : SV_POSITION;
	float4 worldPos : POSITION0;
	float4 normal : NORMAL0;
	float2 uv : TEXCOORD0;
	float4 tangent : TANGENT0;
};

VOut VS(VIn vIn)
{
	VOut output;

	output.position = float4(vIn.position, 1.0);
	output.uv = vIn.uv;

	return output;
}

float4 PS(VOut pIn) : SV_TARGET
{
	float4 diffuse = DiffuseTexture.Sample(DefaultSampler, pIn.uv);

	float4 color;

	float offset[5] = { 0.0,1.0,2.0,3.0,4.0 };
	float weight[5] = { 0.2270270270, 0.1945945946, 0.1216216216, 0.0540540541, 0.0162162162 };

	color = DiffuseTexture.Sample(DefaultSampler, pIn.uv) * weight[0];
	for (int i = 1; i < 5; i++)
	{
		color += DiffuseTexture.Sample(DefaultSampler, (pIn.uv + float2(0.0, offset[i]) / HEIGHT)) * weight[i];
		color += DiffuseTexture.Sample(DefaultSampler, (pIn.uv - float2(0.0, offset[i]) / HEIGHT)) * weight[i];
	}

	return color;
}
