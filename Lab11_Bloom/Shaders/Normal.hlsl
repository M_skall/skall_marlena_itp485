#include "Constants.hlsl"

struct VIn
{
    float3 position : POSITION0;
    float3 normal : NORMAL0;
    float2 uv : TEXCOORD0;
	float3 tangent : TANGENT0;
};

struct VOut
{
    float4 position : SV_POSITION;
    float4 worldPos : POSITION0;
    float4 normal : NORMAL0;
    float2 uv : TEXCOORD0;
	float4 tangent : TANGENT0;
};

VOut VS(VIn vIn)
{
    VOut output;

    // transform input position from model to world space
    output.worldPos = mul(float4(vIn.position, 1.0), c_modelToWorld);
    // transform position from world to projection space
    output.position = mul(output.worldPos, c_viewProj);

    // transform input normal from model to world space
    output.normal = mul(float4(vIn.normal, 0.0), c_modelToWorld);

	// TODO Lab 10b - transform input tangent
	output.tangent = mul(float4(vIn.tangent, 0.0), c_modelToWorld); // is this directed?

    output.uv = vIn.uv;

    return output;
}

float4 PS(VOut pIn) : SV_TARGET
{
    float4 diffuse = DiffuseTexture.Sample(DefaultSampler, pIn.uv);

	// TOOD Lab 10c - calculate binormal
	/*
	Use the normal and tangent you got from the vertex shader (they are in world space) to
	calculate the binormal.
	Don�t forget to normalize all 3 of these
	biasing: color = ( normal + float3( 1, 1, 1 ) ) * 0.5

	from slides:
	float4 PS(VOut pIn) : SV_TARGET
	{
		float4 diffuse = GetDiffuse(pIn); // you figure this part out
		// tangent space
		float3 n = normalize(pIn.normal.xyz);
		float3 t = normalize(pIn.tangent.xyz);
		float3 b = normalize(pIn.binormal.xyz);
		float3x3 TBN = float3x3(t, b, n);
		// read the normal map
		n = NormalTexture.Sample(DefaultSampler, pIn.uv).xyz;
		n = 2.0 * n - 1.0; // bias the normal into -1 to +1 range
		n = mul(n, TBN); // tranform from tangent space to world space
		n = normalize(n); // make sure it�s normalized
		float4 finalColor = DoLighting(diffuse, n); // you figure this part out
		return finalColor;
	}
	*/
	/*
	//Transform tangent, binormal, and normal into world space.
	//Then make matrix
	//Normal, binormal, and tangent are all mutualy perpendicuar
	//Given any 2, we can calculate the 3rd with a cross product
	*/
	float3 n = normalize(pIn.normal.xyz);
	float3 t = normalize(pIn.tangent.xyz);
	float3 binormal = cross(n, t);
	float3 b = normalize(binormal); // does this need to be normalized again?
	float3x3 TBN = float3x3(t, b, n);

	//read the normal map
	n = NormalTexture.Sample(DefaultSampler, pIn.uv).xyz;
	n = 2.0 * n - 1.0; // bias the normal into -1 to +1 range
	n = mul(n, TBN); // tranform from tangent space to world space
	n = normalize(n); // make sure it�s normalized

	//----------------------------------------------------------------------------------

    // do the lighting
    float3 lightColor = c_ambient;
    //float3 n = normalize(pIn.normal.xyz);
    float3 v = normalize(c_cameraPosition - pIn.worldPos);
    for (int i = 0; i < MAX_POINT_LIGHTS; ++i)
    {
        if (c_pointLight[i].isEnabled)
        {
            float3 l = c_pointLight[i].position - pIn.worldPos.xyz;
            float dist = length(l);
            if (dist > 0.0)
            {
                l = l / dist;
                float falloff = smoothstep(c_pointLight[i].outerRadius, c_pointLight[i].innerRadius, dist);
                float3 d = falloff * c_pointLight[i].diffuseColor * max(0.0, dot(l, n));
                lightColor += d;

                float3 r = -reflect(l, n);
                float3 s = falloff * c_pointLight[i].specularColor * pow(max(0.0, dot(r, v)), c_pointLight[i].specularPower);
                lightColor += s;
            }
        }
    }

    float4 finalColor = diffuse * float4(lightColor, 1.0);
    return finalColor;
}
