#include "Constants.hlsl"

struct VIn
{
    float3 position : POSITION0;
    float2 uv : TEXCOORD0;
};

struct VOut
{
    float4 position : SV_POSITION;
    float4 worldPos : POSITION0;
    float4 normal : NORMAL0;
    float2 uv : TEXCOORD0;
	float4 tangent : TANGENT0;
};

VOut VS(VIn vIn)
{
    VOut output;

	// copied, unmodified
	output.position = float4(vIn.position, 1.0);
    output.uv = vIn.uv;

    return output;
}

float4 PS(VOut pIn) : SV_TARGET
{
    float4 diffuse = DiffuseTexture.Sample(DefaultSampler, pIn.uv);

	float4 finalColor = diffuse;
    return finalColor;
}
