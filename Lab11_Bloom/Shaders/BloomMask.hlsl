#include "Constants.hlsl"

struct VIn
{
	float3 position : POSITION0;
	float2 uv : TEXCOORD0;
};

struct VOut
{
	float4 position : SV_POSITION;
	float4 worldPos : POSITION0;
	float4 normal : NORMAL0;
	float2 uv : TEXCOORD0;
	float4 tangent : TANGENT0;
};

VOut VS(VIn vIn)
{
	VOut output;

	output.position = float4(vIn.position, 1.0);
	output.uv = vIn.uv;

	return output;
}

float4 PS(VOut pIn) : SV_TARGET
{
	float4 diffuse = DiffuseTexture.Sample(DefaultSampler, pIn.uv);

	// TODO Lab 11c
	//In the pixel shader, return any �dark� pixels as black and �bright� pixels as their actual color.
	//Let�s define �bright� pixels as any pixel with red, green, or blue value >= 0.8
	if (diffuse.x >= 0.8)
	{
		return diffuse;
	}
	else if (diffuse.y >= 0.8)
	{
		return diffuse;
	}
	else if (diffuse.z >= 0.8)
	{
		return diffuse;
	}

	return float4(0.0, 0.0, 0.0, 0.0);
}
