#pragma once
#include "engineMath.h"

class Graphics;

class Camera
{
public:
    struct PerCameraConstants
    { //think of as pack of 4 floats, 1 pack 4 floats divisible by 4
        Matrix4 c_viewProj;
        Vector3 c_cameraPosition;
		// TODO Lab 11f
		float screenHeight;
		float screenWidth;
        float pad0;
		float pad1;
		float pad2;
    };

    Camera(Graphics* pGraphics);
    ~Camera();
    void SetActive();
    void SetViewMat(const Matrix4& view) { mView = view; }
    void SetProjMat(const Matrix4& proj) { mProj = proj; }

    PerCameraConstants mCameraData;

private:
    Graphics* mGraphics;
    Matrix4 mView;
    Matrix4 mProj;
    ID3D11Buffer* mCameraBuffer;
};