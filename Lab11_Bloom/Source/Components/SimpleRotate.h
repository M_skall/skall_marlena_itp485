#pragma once
#include "Component.h"
#include "engineMath.h"

class SimpleRotate : public Component
{
public:
	SimpleRotate(RenderObj* pObj);

	void LoadProperties(const rapidjson::Value& properties) override;

	void Update(float deltaTime) override;

private:
	float mSpeed;
	float rot;
};