#include "stdafx.h"
#include "Camera.h"
#include "Components\SimpleRotate.h"
#include "Game.h"
#include "jsonUtil.h"
#include "RenderObj.h"
#include "stringUtil.h"

SimpleRotate::SimpleRotate(RenderObj* pObj)
	: Component(pObj)
{

}


//In your LoadProperties(), load a single float property �speed�.
//o In your Update(), make your parent RenderObj rotate around the Z axis at a rate of
//�speed� radians per second
void SimpleRotate::LoadProperties(const rapidjson::Value& properties)
{
	Component::LoadProperties(properties);
	GetFloatFromJSON(properties, "speed", mSpeed);
}

void SimpleRotate::Update(float deltaTime)
{
	this->mObj->mObjectData.c_modelToWorld *= Matrix4::CreateRotationZ(mSpeed * deltaTime);
}