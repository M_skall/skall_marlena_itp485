#include "stdafx.h"
#include "Game.h"
#include "Animation.h"
#include "Camera.h"
#include "engineMath.h"
#include "Graphics.h"
#include "jsonUtil.h"
#include "mesh.h"
#include "Profiler.h"
#include "RenderCube.h"
#include "RenderObj.h"
#include "Shader.h"
#include "Skeleton.h"
#include "SkinnedObj.h"
#include "stringUtil.h"
#include "texture.h"
#include "VertexBuffer.h"
#include "Job.h"
#include "Components\Character.h"
#include "Components\PointLight.h"
#include "Components\Player.h"
#include "Components\FollowCam.h"
#include "Components\CollisionComponent.h"
#include "Components\SimpleRotate.h"
#include "rapidjson\include\rapidjson\rapidjson.h"
#include "rapidjson\include\rapidjson\document.h"
#include <fstream>
#include <sstream>


Game::Game()
    : mShaderCache(this)
    , mTextureCache(this)
    , mMeshCache(this)
    , mSkeletonCache(this)
    , mAnimationCache(this)
{
}

Game::~Game()
{
}

void Game::Init(HWND hWnd, float width, float height)
{
    mGraphics.InitD3D(hWnd, width, height);
    {
        VertexPosColor vert[] =
        {
            { Vector3(0.0f,   0.5f, 0.0f), Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f) },
            { Vector3(0.45f, -0.5,  0.0f), Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f) },
            { Vector3(-0.45f, -0.5f, 0.0f), Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f) }
        };
        uint16_t index[] = { 0, 1, 2 };

        // TODO Lab 02f
        D3D11_INPUT_ELEMENT_DESC inputElem[] =
        {
            { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosColor, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(VertexPosColor, color), D3D11_INPUT_PER_VERTEX_DATA, 0}
        };
        Shader *pShader = new Shader(&mGraphics);
        pShader->Load(L"Shaders/Mesh.hlsl", inputElem, sizeof(inputElem) / sizeof(inputElem[0]));
        mShaderCache.Cache(L"Mesh", pShader);

        VertexBuffer* pVertBuff = new VertexBuffer(&mGraphics,
            vert, sizeof(vert) / sizeof(vert[0]), sizeof(vert[0]),
            index, sizeof(index) / sizeof(index[0]), sizeof(index[0])
            );
        Mesh* pMesh = new Mesh(this, pVertBuff, mShaderCache.Get(L"Mesh"));
        mMeshCache.Cache(L"Triangle", pMesh);
        //RenderObj* pObj = new RenderObj(this, pMesh);
        //mRenderObj.push_back(pObj);
    }
    {
        D3D11_INPUT_ELEMENT_DESC inputElem[] =
        {
            { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNormColorUV, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNormColorUV, norm), D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(VertexPosNormColorUV, color), D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(VertexPosNormColorUV, uv), D3D11_INPUT_PER_VERTEX_DATA, 0 }
        };
        Shader* pShader = new Shader(&mGraphics);
        pShader->Load(L"Shaders/BasicMesh.hlsl", inputElem, sizeof(inputElem) / sizeof(inputElem[0]));
        mShaderCache.Cache(L"BasicMesh", pShader);
    }
    {
        D3D11_INPUT_ELEMENT_DESC inputElem[] =
        {
            { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNormUV, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNormUV, norm), D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(VertexPosNormUV, uv), D3D11_INPUT_PER_VERTEX_DATA, 0 }
        };
        Shader* pPhong = new Shader(&mGraphics);
        pPhong->Load(L"Shaders/Phong.hlsl", inputElem, sizeof(inputElem) / sizeof(inputElem[0]));
        mShaderCache.Cache(L"Phong", pPhong);
        Shader* pUnlit = new Shader(&mGraphics);
        pUnlit->Load(L"Shaders/Unlit.hlsl", inputElem, sizeof(inputElem) / sizeof(inputElem[0]));
        mShaderCache.Cache(L"Unlit", pUnlit);
    }
    {
        D3D11_INPUT_ELEMENT_DESC inputElem[] =
        {
            { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNormBoneUV, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNormBoneUV, norm), D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "BONES", 0, DXGI_FORMAT_R8G8B8A8_UINT, 0, offsetof(VertexPosNormBoneUV, boneIndex), D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "WEIGHTS", 0, DXGI_FORMAT_R8G8B8A8_UNORM, 0, offsetof(VertexPosNormBoneUV, boneWeight), D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(VertexPosNormBoneUV, uv), D3D11_INPUT_PER_VERTEX_DATA, 0 }
        };
        Shader* pShader = new Shader(&mGraphics);
        pShader->Load(L"Shaders/Skinned.hlsl", inputElem, sizeof(inputElem) / sizeof(inputElem[0]));
        mShaderCache.Cache(L"Skinned", pShader);
    }
	// TODO Lab 10b
	{
		D3D11_INPUT_ELEMENT_DESC inputElem[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNormTangent, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNormTangent, norm), D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNormTangent, tangent), D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(VertexPosNormTangent, uv), D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};
		Shader* pShader = new Shader(&mGraphics);
		pShader->Load(L"Shaders/Normal.hlsl", inputElem, sizeof(inputElem) / sizeof(inputElem[0]));
		mShaderCache.Cache(L"Normal", pShader);
	}
	// TODO Lab 11b
	{
		D3D11_INPUT_ELEMENT_DESC inputElem[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexCopyShader, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(VertexCopyShader, uv), D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};
		Shader* pCopy = new Shader(&mGraphics);
		pCopy->Load(L"Shaders/Copy.hlsl", inputElem, sizeof(inputElem) / sizeof(inputElem[0]));
		mShaderCache.Cache(L"Copy", pCopy);

		Shader* pBloom = new Shader(&mGraphics);
		pBloom->Load(L"Shaders/BloomMask.hlsl", inputElem, sizeof(inputElem) / sizeof(inputElem[0]));
		mShaderCache.Cache(L"BloomMask", pBloom);

		Shader* pVertical = new Shader(&mGraphics);
		pVertical->Load(L"Shaders/GaussianVertical.hlsl", inputElem, sizeof(inputElem) / sizeof(inputElem[0]));
		mShaderCache.Cache(L"GaussianVertical", pVertical);

		Shader* pHorizontal = new Shader(&mGraphics);
		pHorizontal->Load(L"Shaders/GaussianHorizontal.hlsl", inputElem, sizeof(inputElem) / sizeof(inputElem[0]));
		mShaderCache.Cache(L"GaussianHorizontal", pHorizontal);

		VertexCopyShader copyVert[] =
		{
			{ Vector3(-1.0f, -1.0f, 0.0f), Vector2(0.0f, 1.0f) },
			{ Vector3(1.0f, -1.0f, 0.0f), Vector2(1.0f, 1.0f) },
			{ Vector3(1.0f, 1.0f, 0.0f), Vector2(1.0f, 0.0f) },
			{ Vector3(-1.0f, 1.0f, 0.0f), Vector2(0.0f, 0.0f) }
		};
		uint16_t copyIndex[] =
		{
			0, 1, 2,
			0, 2, 3
		};

		mCopyVertBuff = new VertexBuffer(&mGraphics,
			copyVert, sizeof(copyVert) / sizeof(copyVert[0]), sizeof(copyVert[0]),
			copyIndex, sizeof(copyIndex) / sizeof(copyIndex[0]), sizeof(copyIndex[0])
		);
	}

    mCamera = new Camera(&mGraphics);
    mLightingBuffer = mGraphics.CreateGraphicsBuffer(&mLightData, sizeof(mLightData), D3D11_BIND_CONSTANT_BUFFER, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);

	// TODO Lab 11a - create render target for full size
	Texture* pTexture = new Texture(&mGraphics);
	mTextureCache.Cache(L"FullRenderTarget", pTexture);
	mRenderTargetView = mTextureCache.Get(L"FullRenderTarget")->CreateRenderTarget(width, height, DXGI_FORMAT_R32G32B32A32_FLOAT);

	// TODO Lab 11d - create render target for half, quarter1, and quarter2
	Texture* halfTex = new Texture(&mGraphics);
	mTextureCache.Cache(L"HalfRenderTarget", halfTex);
	mHalfRTV = mTextureCache.Get(L"HalfRenderTarget")->CreateRenderTarget(width/2.0, height/2.0, DXGI_FORMAT_R32G32B32A32_FLOAT);
	Texture* q1Tex = new Texture(&mGraphics);
	mTextureCache.Cache(L"QuarterOneRenderTarget", q1Tex);
	mQuarterRTV_1 = mTextureCache.Get(L"QuarterOneRenderTarget")->CreateRenderTarget(width/4.0, height/4.0, DXGI_FORMAT_R32G32B32A32_FLOAT);
	Texture* q2Tex = new Texture(&mGraphics);
	mTextureCache.Cache(L"QuarterTwoRenderTarget", q2Tex);
	mQuarterRTV_2 = mTextureCache.Get(L"QuarterTwoRenderTarget")->CreateRenderTarget(width/4.0, height/4.0, DXGI_FORMAT_R32G32B32A32_FLOAT);

	// TODO Lab 11f
	D3D11_BLEND srcBlendO;
	ZeroMemory(&srcBlendO, sizeof(D3D11_BLEND));
	srcBlendO = D3D11_BLEND_ZERO;
	D3D11_BLEND dstBlendO;
	ZeroMemory(&dstBlendO, sizeof(D3D11_BLEND));
	dstBlendO = D3D11_BLEND_ZERO;

	D3D11_BLEND srcBlend;
	ZeroMemory(&srcBlend, sizeof(D3D11_BLEND));
	srcBlend = D3D11_BLEND_ONE;
	D3D11_BLEND dstBlend;
	ZeroMemory(&dstBlend, sizeof(D3D11_BLEND));
	dstBlend = D3D11_BLEND_ONE;
	//dstBlend = D3D11_BLEND_SRC_ALPHA * D3D11_BLEND_SRC_COLOR + D3D11_BLEND_DEST_COLOR;	
	mOpaque = mGraphics.CreateBlendState(false, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO);
	mAdditive = mGraphics.CreateBlendState(true, D3D11_BLEND_ONE, D3D11_BLEND_ONE);


    LoadLevel(L"Assets/Levels/Level10.itplevel");
	mJobManager.Begin();
}

void Game::Shutdown()
{
	ID3D11Debug* DebugDevice = nullptr;
	mGraphics.GetDevice()->QueryInterface(__uuidof(ID3D11Debug), reinterpret_cast <void**>(&DebugDevice));

    mGraphics.CleanD3D();
    delete mCamera;
	// TODO Lab 11a+11d
	mOpaque->Release();
	mAdditive->Release();
	mRenderTargetView->Release();
	mHalfRTV->Release();
	mQuarterRTV_1->Release();
	mQuarterRTV_2->Release();
	delete mCopyVertBuff;
	mLightingBuffer->Release();
    for (RenderObj* pObj : mRenderObj)
        delete pObj;
	mJobManager.End();

	// clear on all caches
	mTextureCache.Clear();
	mShaderCache.Clear();
	mMeshCache.Clear();
	mSkeletonCache.Clear();
	mAnimationCache.Clear();


	DebugDevice->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL| D3D11_RLDO_IGNORE_INTERNAL);
	DebugDevice->Release();
}

static float s_angle = 0.0f;
void Game::Update(float deltaTime)
{
    for (RenderObj* pObj : mRenderObj)
    {
        pObj->Update(deltaTime);
    }
	mJobManager.WaitForJobs();
}

void Game::RenderFrame()
{
	// TODO Lab 11f
	mGraphics.SetBlendState(mOpaque);

	// TODO Lab 11a
	// modify new render target as the render target
	mGraphics.SetRenderTarget(mRenderTargetView, mGraphics.GetDepthBuffer());
    {
        PROFILE_SCOPE(Render_SetUp);
        Graphics::Color4 clearColor(0.0f, 0.2f, 0.4f, 1.0f);
        mGraphics.BeginFrame(clearColor);
        mCamera->SetActive();
        mGraphics.UploadBuffer(mLightingBuffer, &mLightData, sizeof(mLightData));
        mGraphics.GetDeviceContext()->PSSetConstantBuffers(Graphics::CONSTANT_BUFFER_LIGHTING, 1, &mLightingBuffer);
    }
    {
        PROFILE_SCOPE(Render_Objects);
        for (RenderObj* pObj : mRenderObj)
            pObj->Draw();
    }

	// TOOD Lab 11d - from full to half
	mGraphics.SetViewport(0.0, 0.0, mGraphics.GetScreenWidth() / 2.0, mGraphics.GetScreenHeight() / 2.0);
	mGraphics.SetRenderTarget(mHalfRTV, nullptr);
	// use Copy shader to copy from offscreen texture to render target (back buffer)
	mTextureCache.Get(L"FullRenderTarget")->SetActive(Graphics::TEXTURE_SLOT_DIFFUSE);
	GetShader(L"BloomMask")->SetActive();
	mCopyVertBuff->SetActive();
	mCopyVertBuff->Draw();
	ID3D11ShaderResourceView* clear = NULL;
	mGraphics.GetDeviceContext()->PSSetShaderResources(0, 1, &clear);

	// from half to quarter
	mGraphics.SetViewport(0.0, 0.0, mGraphics.GetScreenWidth() / 4.0, mGraphics.GetScreenHeight() / 4.0);
	mGraphics.SetRenderTarget(mQuarterRTV_1, nullptr);
	// use Copy shader to copy from offscreen texture to render target (back buffer)
	mTextureCache.Get(L"HalfRenderTarget")->SetActive(Graphics::TEXTURE_SLOT_DIFFUSE);
	GetShader(L"Copy")->SetActive();
	mCopyVertBuff->SetActive();
	mCopyVertBuff->Draw();
	clear = NULL;
	mGraphics.GetDeviceContext()->PSSetShaderResources(0, 1, &clear);
	
	// TODO Lab 11e??
	// separate our Gaussian filter into a horizontal blur filter (q1) and the vertical blur filter (q2)
	// screen resolution = screenwidth * screenheight
	// Do the horizontal pass from quarter#1 onto quarter#2
	mGraphics.SetViewport(0.0, 0.0, mGraphics.GetScreenWidth() / 4.0, mGraphics.GetScreenHeight() / 4.0);
	mGraphics.SetRenderTarget(mQuarterRTV_2, nullptr);
	mTextureCache.Get(L"QuarterOneRenderTarget")->SetActive(Graphics::TEXTURE_SLOT_DIFFUSE);
	GetShader(L"GaussianHorizontal")->SetActive();
	mCopyVertBuff->SetActive();
	mCopyVertBuff->Draw();
	clear = NULL;
	mGraphics.GetDeviceContext()->PSSetShaderResources(0, 1, &clear);

	//careful not to set a render target while you still have its texture mapped
	mGraphics.SetActiveTexture(0, nullptr); 

	// Do the vertical pass from quarter#2 onto quarter#1
	mGraphics.SetViewport(0.0, 0.0, mGraphics.GetScreenWidth() / 4.0, mGraphics.GetScreenHeight() / 4.0);
	mGraphics.SetRenderTarget(mQuarterRTV_1, nullptr);
	mTextureCache.Get(L"QuarterTwoRenderTarget")->SetActive(Graphics::TEXTURE_SLOT_DIFFUSE);
	GetShader(L"GaussianVertical")->SetActive();
	mCopyVertBuff->SetActive();
	mCopyVertBuff->Draw();
	clear = NULL;
	mGraphics.GetDeviceContext()->PSSetShaderResources(0, 1, &clear);

	// from result (quarter) to back buffer
	mGraphics.SetViewport(0.0, 0.0, mGraphics.GetScreenWidth(), mGraphics.GetScreenHeight());
	mGraphics.SetRenderTarget(mGraphics.GetBackBuffer(), nullptr);
	mTextureCache.Get(L"QuarterOneRenderTarget")->SetActive(Graphics::TEXTURE_SLOT_DIFFUSE);
	GetShader(L"Copy")->SetActive();
	mCopyVertBuff->SetActive();
	mCopyVertBuff->Draw();
	clear = NULL;
	mGraphics.GetDeviceContext()->PSSetShaderResources(0, 1, &clear);
	
	// TODO Lab 11f
	mGraphics.SetBlendState(mAdditive);

	// TODO Lab 11f - copy the original off-screen buffer onto the back buffer.
	mGraphics.SetViewport(0.0, 0.0, mGraphics.GetScreenWidth(), mGraphics.GetScreenHeight());
	//mGraphics.SetRenderTarget(mRenderTargetView, mGraphics.GetDepthBuffer());
	mGraphics.SetRenderTarget(mGraphics.GetBackBuffer(), nullptr);
	mTextureCache.Get(L"FullRenderTarget")->SetActive(Graphics::TEXTURE_SLOT_DIFFUSE);
	GetShader(L"Copy")->SetActive();
	mCopyVertBuff->SetActive();
	mCopyVertBuff->Draw();
	clear = NULL;
	mGraphics.GetDeviceContext()->PSSetShaderResources(0, 1, &clear);

	{
        PROFILE_SCOPE(Render_End);
        mGraphics.EndFrame();
    }
}

void Game::OnKeyDown(uint32_t key)
{
	m_keyIsHeld[key] = true;
}

void Game::OnKeyUp(uint32_t key)
{
	m_keyIsHeld[key] = false;
}

bool Game::IsKeyHeld(uint32_t key) const
{
	const auto find = m_keyIsHeld.find(key);
	if (find != m_keyIsHeld.end())
		return find->second;
	return false;
}

Game::PointLightData* Game::AllocateLight()
{
    for (int i = 0; i < MAX_POINT_LIGHTS; ++i)
    {
        if (false == mLightData.c_pointLight[i].isEnabled)
        {
            mLightData.c_pointLight[i].isEnabled = true;
            return &mLightData.c_pointLight[i];
        }
    }
    return nullptr;
}

void Game::FreeLight(PointLightData* pLight)
{
    pLight->isEnabled = false;
}

Shader* Game::GetShader(const std::wstring& shaderName)
{
    return mShaderCache.Get(shaderName);
}

Texture* Game::LoadTexture(const std::wstring& fileName)
{
    return mTextureCache.Load(fileName);
}

Mesh* Game::LoadMesh(const std::wstring& fileName)
{
    return mMeshCache.Load(fileName);
}

Skeleton* Game::LoadSkeleton(const std::wstring& fileName)
{
    return mSkeletonCache.Load(fileName);
}

Animation* Game::LoadAnimation(const std::wstring& fileName)
{
    return mAnimationCache.Load(fileName);
}

bool Game::LoadLevel(const WCHAR* fileName)
{
	std::ifstream file(fileName);
	if (!file.is_open())
	{
		return false;
	}

	std::stringstream fileStream;
	fileStream << file.rdbuf();
	std::string contents = fileStream.str();
	rapidjson::StringStream jsonStr(contents.c_str());
	rapidjson::Document doc;
	doc.ParseStream(jsonStr);

	if (!doc.IsObject())
	{
		return false;
	}

	std::string str = doc["metadata"]["type"].GetString();
	int ver = doc["metadata"]["version"].GetInt();

	// Check the metadata
	if (!doc["metadata"].IsObject() ||
		str != "itplevel" ||
		ver != 2)
	{
		return false;
	}

    const rapidjson::Value& camera = doc["camera"];
    if (camera.IsObject())
    {
        Vector3 pos;
        GetVectorFromJSON(camera, "position", pos);
        Quaternion rot;
        GetQuaternionFromJSON(camera, "rotation", rot);
        Matrix4 mat = Matrix4::CreateFromQuaternion(rot) * Matrix4::CreateTranslation(pos);
        mat.Invert();
        mCamera->SetViewMat(mat);
    }

    const rapidjson::Value& lighting = doc["lightingData"];
    if (lighting.IsObject())
    {
        Vector3 light;
        GetVectorFromJSON(lighting, "ambient", light);
        SetAmbientLight(light);
    }

    const rapidjson::Value& renderObjects = doc["renderObjects"];
    if (renderObjects.IsArray())
    {
        for (rapidjson::SizeType i = 0; i < renderObjects.Size(); ++i)
        {
            const rapidjson::Value& obj = renderObjects[i];
            Vector3 pos;
            GetVectorFromJSON(obj, "position", pos);
            Quaternion rot;
            GetQuaternionFromJSON(obj, "rotation", rot);
            float scale;
            GetFloatFromJSON(obj, "scale", scale);
            std::wstring mesh;
            GetWStringFromJSON(obj, "mesh", mesh);
            Mesh* pMesh = LoadMesh(mesh);
            RenderObj* pObj = nullptr;
            SkinnedObj* pSkin = nullptr;
            if (nullptr != pMesh && pMesh->IsSkinned())
                pObj = pSkin = new SkinnedObj(this, pMesh);
            else
                pObj = new RenderObj(this, pMesh);
            pObj->mObjectData.c_modelToWorld = 
                Matrix4::CreateScale(scale)
                * Matrix4::CreateFromQuaternion(rot)
                * Matrix4::CreateTranslation(pos);

            const rapidjson::Value& components = obj["components"];
            if (components.IsArray())
            {
                for (rapidjson::SizeType j = 0; j < components.Size(); ++j)
                {
                    Component* pComp = nullptr;
                    const rapidjson::Value& comp = components[j];
                    std::string type;
                    GetStringFromJSON(comp, "type", type);
                    if (type == "PointLight")
                    {
                        pComp = new PointLight(pObj);
                    }
                    else if (type == "Character")
                    {
                        pComp = new Character(pSkin);
                    }
					else if (type == "Player")
					{
						pComp = new Player(pSkin);
					}
					else if (type == "FollowCam")
					{
						pComp = new FollowCam(pObj);
					}
					else if (type == "Collision")
					{
						pComp = new CollisionComponent(pObj);
					}
					else if (type == "SimpleRotate")
					{
						pComp = new SimpleRotate(pObj);
					}
                    if (nullptr != pComp)
                    {
                        pComp->LoadProperties(comp);
                        pObj->AddComponent(pComp);
                    }
                }
            }

            mRenderObj.push_back(pObj);
        }
    }


	return true;
}