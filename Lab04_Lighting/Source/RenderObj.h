#pragma once
#include "engineMath.h"

class Game;
class Shader;
class VertexBuffer;

class RenderObj
{
public:
    struct PerObjectConstants
    {
        Matrix4 c_modelToWorld;
    };

    RenderObj(Game *pGame,
        const VertexBuffer *vertexBuffer, const Shader *shader);
    virtual ~RenderObj();
    virtual void Draw();

    PerObjectConstants mObjectData;

protected:
    Game* mGame;
    const VertexBuffer* mVertexBuffer;
    const Shader* mShader;
    ID3D11Buffer* mObjectBuffer;
};
