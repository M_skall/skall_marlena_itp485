#include "stdafx.h"
#include "RenderCube.h"
#include "game.h"
#include "VertexBuffer.h"

static VertexPosColor cubeVertex[] =
{
    { Vector3(-0.5f, 0.5f, -0.5f),	Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f) },
    { Vector3(0.5f, 0.5f, -0.5f),	Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f) },
    { Vector3(0.5f, -0.5f, -0.5f),	Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f) },
    { Vector3(-0.5f, -0.5f, -0.5f),	Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f) },

    { Vector3(0.5f, 0.5f, -0.5f),	Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f) },
    { Vector3(0.5f, 0.5f, 0.5f),	Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f) },
    { Vector3(0.5f, -0.5f, 0.5f),	Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f) },
    { Vector3(0.5f, -0.5f, -0.5f),	Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f) },

    { Vector3(-0.5f, -0.5f, 0.5f),	Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f) },
    { Vector3(0.5f, -0.5f, 0.5f),	Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f) },
    { Vector3(0.5f, 0.5f, 0.5f),	Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f) },
    { Vector3(-0.5f, 0.5f, 0.5f),	Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f) },

    { Vector3(-0.5f, -0.5f, -0.5f),	Graphics::Color4(1.0f, 1.0f, 0.0f, 1.0f) },
    { Vector3(-0.5f, -0.5f, 0.5f),	Graphics::Color4(1.0f, 1.0f, 0.0f, 1.0f) },
    { Vector3(-0.5f, 0.5f, 0.5f),	Graphics::Color4(1.0f, 1.0f, 0.0f, 1.0f) },
    { Vector3(-0.5f, 0.5f, -0.5f),	Graphics::Color4(1.0f, 1.0f, 0.0f, 1.0f) },

    { Vector3(-0.5f, -0.5f, -0.5f),	Graphics::Color4(0.0f, 1.0f, 1.0f, 1.0f) },
    { Vector3(0.5f, -0.5f, -0.5f),	Graphics::Color4(0.0f, 1.0f, 1.0f, 1.0f) },
    { Vector3(0.5f, -0.5f, 0.5f),	Graphics::Color4(0.0f, 1.0f, 1.0f, 1.0f) },
    { Vector3(-0.5f, -0.5f, 0.5f),	Graphics::Color4(0.0f, 1.0f, 1.0f, 1.0f) },

    { Vector3(-0.5f, 0.5f, 0.5f),	Graphics::Color4(1.0f, 0.0f, 1.0f, 1.0f) },
    { Vector3(0.5f, 0.5f, 0.5f),	Graphics::Color4(1.0f, 0.0f, 1.0f, 1.0f) },
    { Vector3(0.5f, 0.5f, -0.5f),	Graphics::Color4(1.0f, 0.0f, 1.0f, 1.0f) },
    { Vector3(-0.5f, 0.5f, -0.5f),	Graphics::Color4(1.0f, 0.0f, 1.0f, 1.0f) },
};
static uint16_t cubeIndex[] =
{
    2, 1, 0,
    3, 2, 0,

    6, 5, 4,
    7, 6, 4,

    10, 9, 8,
    11, 10, 8,

    14, 13, 12,
    15, 14, 12,

    18, 17, 16,
    19, 18, 16,

    22, 21, 20,
    23, 22, 20,
};

static VertexPosColorUV cubeUV[] =
{
	//face1
	//{ Vector3(0.0f, 0.5f, 0.0f), Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f), Vector3(0.5, 0.0, 1.0)},
	//	{ Vector3(0.45f, -0.5,  0.0f), Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f), Vector3(0.0, 1.0, 1.0) },
	//	{ Vector3(-0.45f, -0.5f, 0.0f), Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f), Vector3(1.0, 1.0, 1.0)}

	// back face - RED
	/*
	0----1
	|	 |
	3----2
	*/
	{ Vector3(-0.5f, 0.5f, -0.5f),	Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f), Vector2(1.0, 0.0) }, //0
	{ Vector3(0.5f, 0.5f, -0.5f),	Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f), Vector2(0.0, 0.0) }, //1
	{ Vector3(0.5f, -0.5f, -0.5f),	Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f), Vector2(0.0, 1.0) }, //2 
	{ Vector3(-0.5f, -0.5f, -0.5f),	Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f), Vector2(1.0, 1.0) }, //3

	// right face - GREEN
	/*
	5----4
	|	 |
	6----7
	*/
	{ Vector3(0.5f, 0.5f, -0.5f),	Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f), Vector2(1.0, 0.0) }, //4
	{ Vector3(0.5f, 0.5f, 0.5f),	Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f), Vector2(0.0, 0.0) }, //5
	{ Vector3(0.5f, -0.5f, 0.5f),	Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f), Vector2(0.0, 1.0) }, //6
	{ Vector3(0.5f, -0.5f, -0.5f),	Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f), Vector2(1.0, 1.0) }, //7

	// front face - BLUE
	/*
	11----10
	|	  |
	8-----9
	*/
	{ Vector3(-0.5f, -0.5f, 0.5f),	Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f), Vector2(0.0, 1.0) }, //8
	{ Vector3(0.5f, -0.5f, 0.5f),	Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f), Vector2(1.0, 1.0) }, //9
	{ Vector3(0.5f, 0.5f, 0.5f),	Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f), Vector2(1.0, 0.0) }, //10
	{ Vector3(-0.5f, 0.5f, 0.5f),	Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f), Vector2(0.0, 0.0) }, //11

	// left face - YELLOW
	/*
	15----14
	|	  |
	12----13
	*/
	{ Vector3(-0.5f, -0.5f, -0.5f),	Graphics::Color4(1.0f, 1.0f, 0.0f, 1.0f), Vector2(0.0, 1.0) }, //12
	{ Vector3(-0.5f, -0.5f, 0.5f),	Graphics::Color4(1.0f, 1.0f, 0.0f, 1.0f), Vector2(1.0, 1.0) }, //13
	{ Vector3(-0.5f, 0.5f, 0.5f),	Graphics::Color4(1.0f, 1.0f, 0.0f, 1.0f), Vector2(1.0, 0.0) }, //14
	{ Vector3(-0.5f, 0.5f, -0.5f),	Graphics::Color4(1.0f, 1.0f, 0.0f, 1.0f), Vector2(0.0, 0.0) }, //15

	// bottom face - CYAN
	/*
	16----17
	|	  |
	19----18
	*/
	{ Vector3(-0.5f, -0.5f, -0.5f),	Graphics::Color4(0.0f, 1.0f, 1.0f, 1.0f), Vector2(1.0, 0.0) }, //16
	{ Vector3(0.5f, -0.5f, -0.5f),	Graphics::Color4(0.0f, 1.0f, 1.0f, 1.0f), Vector2(0.0, 0.0) }, //17
	{ Vector3(0.5f, -0.5f, 0.5f),	Graphics::Color4(0.0f, 1.0f, 1.0f, 1.0f), Vector2(0.0, 1.0) }, //18
	{ Vector3(-0.5f, -0.5f, 0.5f),	Graphics::Color4(0.0f, 1.0f, 1.0f, 1.0f), Vector2(1.0, 1.0) }, //19

	// top face - PINK
	/*
	23----22
	|	  |
	20----21
	*/
	{ Vector3(-0.5f, 0.5f, 0.5f),	Graphics::Color4(1.0f, 0.0f, 1.0f, 1.0f), Vector2(0.0, 1.0) }, //20
	{ Vector3(0.5f, 0.5f, 0.5f),	Graphics::Color4(1.0f, 0.0f, 1.0f, 1.0f), Vector2(1.0, 1.0) }, //21
	{ Vector3(0.5f, 0.5f, -0.5f),	Graphics::Color4(1.0f, 0.0f, 1.0f, 1.0f), Vector2(1.0, 0.0) }, //22
	{ Vector3(-0.5f, 0.5f, -0.5f),	Graphics::Color4(1.0f, 0.0f, 1.0f, 1.0f), Vector2(0.0, 0.0) }  //23
};

static VertexPosColorUVN cubeUVN[] =
{
	//face1
	//{ Vector3(0.0f, 0.5f, 0.0f), Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f), Vector3(0.5, 0.0, 1.0)},
	//	{ Vector3(0.45f, -0.5,  0.0f), Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f), Vector3(0.0, 1.0, 1.0) },
	//	{ Vector3(-0.45f, -0.5f, 0.0f), Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f), Vector3(1.0, 1.0, 1.0)}

	// back face - RED
	/*
	0----1
	|	 |
	3----2
	*/
	{ Vector3(-0.5f, 0.5f, -0.5f),	Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f), Vector2(1.0, 0.0), Vector3(-1.0f, 0.0f, 0.0f) }, //0
	{ Vector3(0.5f, 0.5f, -0.5f),	Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f), Vector2(0.0, 0.0), Vector3(-1.0f, 0.0f, 0.0f) }, //1
	{ Vector3(0.5f, -0.5f, -0.5f),	Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f), Vector2(0.0, 1.0), Vector3(-1.0f, 0.0f, 0.0f) }, //2 
	{ Vector3(-0.5f, -0.5f, -0.5f),	Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f), Vector2(1.0, 1.0), Vector3(-1.0f, 0.0f, 0.0f) }, //3

	// right face - GREEN
	/*
	5----4
	|	 |
	6----7
	*/
	{ Vector3(0.5f, 0.5f, -0.5f),	Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f), Vector2(1.0, 0.0), Vector3(1.0f, 0.0f, 0.0f) }, //4
	{ Vector3(0.5f, 0.5f, 0.5f),	Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f), Vector2(0.0, 0.0), Vector3(1.0f, 0.0f, 0.0f) }, //5
	{ Vector3(0.5f, -0.5f, 0.5f),	Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f), Vector2(0.0, 1.0), Vector3(1.0f, 0.0f, 0.0f) }, //6
	{ Vector3(0.5f, -0.5f, -0.5f),	Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f), Vector2(1.0, 1.0), Vector3(1.0f, 0.0f, 0.0f) }, //7

	// front face - BLUE
	/*
	11----10
	|	  |
	8-----9
	*/
	{ Vector3(-0.5f, -0.5f, 0.5f),	Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f), Vector2(0.0, 1.0), Vector3(1.0f, 0.0f, 0.0f) }, //8
	{ Vector3(0.5f, -0.5f, 0.5f),	Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f), Vector2(1.0, 1.0), Vector3(1.0f, 0.0f, 0.0f) }, //9
	{ Vector3(0.5f, 0.5f, 0.5f),	Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f), Vector2(1.0, 0.0), Vector3(1.0f, 0.0f, 0.0f) }, //10
	{ Vector3(-0.5f, 0.5f, 0.5f),	Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f), Vector2(0.0, 0.0), Vector3(1.0f, 0.0f, 0.0f) }, //11

	// left face - YELLOW
	/*
	15----14
	|	  |
	12----13
	*/
	{ Vector3(-0.5f, -0.5f, -0.5f),	Graphics::Color4(1.0f, 1.0f, 0.0f, 1.0f), Vector2(0.0, 1.0), Vector3(-1.0f, 0.0f, 0.0f) }, //12
	{ Vector3(-0.5f, -0.5f, 0.5f),	Graphics::Color4(1.0f, 1.0f, 0.0f, 1.0f), Vector2(1.0, 1.0), Vector3(-1.0f, 0.0f, 0.0f) }, //13
	{ Vector3(-0.5f, 0.5f, 0.5f),	Graphics::Color4(1.0f, 1.0f, 0.0f, 1.0f), Vector2(1.0, 0.0), Vector3(-1.0f, 0.0f, 0.0f) }, //14
	{ Vector3(-0.5f, 0.5f, -0.5f),	Graphics::Color4(1.0f, 1.0f, 0.0f, 1.0f), Vector2(0.0, 0.0), Vector3(-1.0f, 0.0f, 0.0f) }, //15

	// bottom face - CYAN
	/*
	16----17
	|	  |
	19----18
	*/
	{ Vector3(-0.5f, -0.5f, -0.5f),	Graphics::Color4(0.0f, 1.0f, 1.0f, 1.0f), Vector2(1.0, 0.0), Vector3(0.0f, -1.0f, 0.0f) }, //16
	{ Vector3(0.5f, -0.5f, -0.5f),	Graphics::Color4(0.0f, 1.0f, 1.0f, 1.0f), Vector2(0.0, 0.0), Vector3(0.0f, -1.0f, 0.0f) }, //17
	{ Vector3(0.5f, -0.5f, 0.5f),	Graphics::Color4(0.0f, 1.0f, 1.0f, 1.0f), Vector2(0.0, 1.0), Vector3(0.0f, -1.0f, 0.0f) }, //18
	{ Vector3(-0.5f, -0.5f, 0.5f),	Graphics::Color4(0.0f, 1.0f, 1.0f, 1.0f), Vector2(1.0, 1.0), Vector3(0.0f, -1.0f, 0.0f) }, //19

	// top face - PINK
	/*
	23----22
	|	  |
	20----21
	*/
	{ Vector3(-0.5f, 0.5f, 0.5f),	Graphics::Color4(1.0f, 0.0f, 1.0f, 1.0f), Vector2(0.0, 1.0), Vector3(0.0f, 1.0f, 0.0f) }, //20
	{ Vector3(0.5f, 0.5f, 0.5f),	Graphics::Color4(1.0f, 0.0f, 1.0f, 1.0f), Vector2(1.0, 1.0), Vector3(0.0f, 1.0f, 0.0f) }, //21
	{ Vector3(0.5f, 0.5f, -0.5f),	Graphics::Color4(1.0f, 0.0f, 1.0f, 1.0f), Vector2(1.0, 0.0), Vector3(0.0f, 1.0f, 0.0f) }, //22
	{ Vector3(-0.5f, 0.5f, -0.5f),	Graphics::Color4(1.0f, 0.0f, 1.0f, 1.0f), Vector2(0.0, 0.0), Vector3(0.0f, 1.0f, 0.0f) }  //23
};


RenderCube::RenderCube(Game* pGame, const Shader* shader)
    : RenderObj(pGame, nullptr, shader)
{
    mVertexBuffer = new VertexBuffer(mGame->GetGraphics(),
		cubeUVN, sizeof(cubeUVN)/sizeof(cubeUVN[0]), sizeof(cubeUVN[0]),
        cubeIndex, sizeof(cubeIndex)/sizeof(cubeIndex[0]), sizeof(cubeIndex[0])
        );
}