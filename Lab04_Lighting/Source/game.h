#pragma once
#include "Graphics.h"
#include "engineMath.h"

class Camera;
class Shader;
class RenderObj;
class Texture;

struct PointLightData
{
	Vector3 diffuseColor;
	float specularPower;
	Vector3 specularColor;
	float innerRadius;
	Vector3 position;
	float outerRadius;
	bool isEnabled;
	Vector3 padding;
};

class Game
{
public:
	struct LightingData
	{
		Vector3 c_ambient;
		float padding;
		PointLightData c_pointLight[8];
	};

    Game();
    ~Game();

    void Init(HWND hWnd, float width, float height);
    void Shutdown();
	void Update(float deltaTime);
    void RenderFrame();

	void OnKeyDown(uint32_t key);
	void OnKeyUp(uint32_t key);
	bool IsKeyHeld(uint32_t key) const;

	// TODO Lab 02b
    Graphics* GetGraphics() { return &mGraphics; }

	// TODO Lab 04d
	PointLightData* AllocateLight();
	void FreeLight(PointLightData* pLight);
	void SetAmbientLight(const Vector3& color);
	const Vector3& GetAmbientLight() const;

private:
	std::unordered_map<uint32_t, bool> m_keyIsHeld;
	// TODO Lab 02b
    Graphics mGraphics;
	// TODO Lab 02e
	// TODO Lab 02f
    Shader* mSimpleShader;
	// TODO Lab 04c
	Shader* mBasicMeshShader;
	Texture* mTexture;

    RenderObj* mTriangle;
    RenderObj* mCube;
    Camera* mCamera;

	bool LoadLevel(const WCHAR* fileName);

	// TODO Lab 04d
	LightingData lightingData;
	ID3D11Buffer* mLightingBuffer;
};

struct VertexPosColor
{
    Vector3 pos;
    Graphics::Color4 color;
};

struct VertexPosColorUV
{
	Vector3 pos;
	Graphics::Color4 color;
	Vector2 uv;
};

struct VertexPosColorUVN
{
	Vector3 pos;
	Graphics::Color4 color;
	Vector2 uv;
	Vector3 normal;
};

/*
Watch out for padding. Each float3 needs a 4-byte pad after it unless there�s a 4-byte
(or less) variable immediately following it that can take that spot.
o Declare a constant for the number of point lights, and make sure it matches the one
defined in Constants.hlsl. */