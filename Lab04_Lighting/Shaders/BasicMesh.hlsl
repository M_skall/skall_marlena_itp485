#include "Constants.hlsl"

struct VIn
{
    float3 position : POSITION0;
    float4 color : COLOR0;
	float2 uv : UV0;
	float3 normal : NORMAL0;
};

struct VOut
{
    float4 position : SV_POSITION;
    float4 color : COLOR0;
	float2 uv : UV0;
	float3 normal : NORMAL0;
	float4 worldPosition : POS1;
};

VOut VS(VIn vIn)
{
    VOut output;

    // transform input position from model to world space
    output.position = mul(float4(vIn.position, 1.0), c_modelToWorld);
    // transform position from world to projection space
    output.position = mul(output.position, c_viewProj);

    output.color = vIn.color;

	output.uv = vIn.uv;

	output.worldPosition = mul(float4(vIn.position, 1.0), c_modelToWorld);

	//Use the c_modelToWorld matrix to transform the normal. Don�t forget it�s a direction vector.
	output.normal = mul(float4(vIn.normal, 0.0), c_modelToWorld).xyz;
    
	return output;
}

float4 PS(VOut pIn) : SV_TARGET
{
	float4 sampleDiffuse = DiffuseTexture.Sample(DefaultSampler, pIn.uv);

	float3 lightColor = c_ambient;
	//pIn.worldPosition = mul(pIn.position, c_modelToWorld).xyz;
	float3 n = normalize(pIn.normal);

	float3 v = normalize(c_cameraPosition - pIn.worldPosition.xyz);
	//max(v, 0.0f);
	for (int i = 0; i < 8; i++) {
		if (c_pointLight[i].isEnabled == true) {
			float3 l = normalize(c_pointLight[i].position - pIn.worldPosition.xyz);
			float3 len = length(l); /* lightColor - c_pointLight[i].position;*/
			float3 r = reflect(-l,n);

			/*
			Calculate diffuse based on dot(n, l)
			� Use max() to ensure it does not go negative.
			� Multiply this by the falloff and the diffuse color.
			� Calculate specular components based on pow(dot(r, v), specularPower)
			� Again use max() to ensure it does not go negative.
			� Multiply this by the falloff and the diffuse color.
*/
			lightColor += c_pointLight[i].diffuseColor * max(dot(n, l), 0.0f) * smoothstep(c_pointLight[i].outerRadius, c_pointLight[i].innerRadius, len);
			lightColor += c_pointLight[i].specularColor * pow(max(dot(r, v), 0.0f), c_pointLight[i].specularPower) * smoothstep(c_pointLight[i].outerRadius, c_pointLight[i].innerRadius, len);
		}
	}

    return pIn.color * float4(lightColor, 0.0f) * sampleDiffuse;
}
