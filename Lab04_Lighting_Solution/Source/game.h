#pragma once
#include "Graphics.h"
#include "engineMath.h"

class Camera;
class RenderObj;
class Shader;
class Texture;

#define MAX_POINT_LIGHTS 8

class Game
{
public:
    struct PointLightData
    {
        Vector3 diffuseColor;
        float pad0;
        Vector3 specularColor;
        float pad1;
        Vector3 position;
        float specularPower;
        float innerRadius;
        float outerRadius;
        bool isEnabled;
        float pad2;
    };

    struct LightingData
    {
        Vector3	c_ambient;
        float pad;
        PointLightData c_pointLight[MAX_POINT_LIGHTS];
    };

    Game();
    ~Game();

    void Init(HWND hWnd, float width, float height);
    void Shutdown();
	void Update(float deltaTime);
    void RenderFrame();

	void OnKeyDown(uint32_t key);
	void OnKeyUp(uint32_t key);
	bool IsKeyHeld(uint32_t key) const;

	// TODO Lab 02b
    Graphics* GetGraphics() { return &mGraphics; }

    PointLightData* AllocateLight();
    void FreeLight(PointLightData* pLight);
    void SetAmbientLight(const Vector3& color) { mLightData.c_ambient = color; }
    const Vector3 &GetAmbientLight() const { return mLightData.c_ambient; }

private:
	std::unordered_map<uint32_t, bool> m_keyIsHeld;
	// TODO Lab 02b
    Graphics mGraphics;
	// TODO Lab 02e
	// TODO Lab 02f
    Shader* mSimpleShader;
    Shader* mBasicMeshShader;
    Texture* mTexture;

    RenderObj* mTriangle;
    RenderObj* mCube;
    Camera* mCamera;
    LightingData mLightData;
    ID3D11Buffer *mLightingBuffer;

	bool LoadLevel(const WCHAR* fileName);
};

struct VertexPosColor
{
    Vector3 pos;
    Graphics::Color4 color;
};

struct VertexPosColorUV
{
    Vector3 pos;
    Graphics::Color4 color;
    Vector2 uv;
};

struct VertexPosNormColorUV
{
    Vector3 pos;
    Vector3 norm;
    Graphics::Color4 color;
    Vector2 uv;
};
