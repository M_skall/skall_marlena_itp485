#pragma once
#include "engineMath.h"

class Graphics;

class Camera
{
public:
    struct PerCameraConstants
    {
        Matrix4 c_viewProj;
        Vector3 c_cameraPosition;
        float pad;
    };

    Camera(Graphics* pGraphics);
    ~Camera();
    void SetActive();

    PerCameraConstants mCameraData;

private:
    Graphics* mGraphics;
    Matrix4 mView;
    Matrix4 mProj;
    ID3D11Buffer* mCameraBuffer;
};