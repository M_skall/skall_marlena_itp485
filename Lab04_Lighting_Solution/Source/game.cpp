#include "stdafx.h"
#include "Game.h"
#include "Camera.h"
#include "engineMath.h"
#include "Graphics.h"
#include "RenderCube.h"
#include "RenderObj.h"
#include "Shader.h"
#include "stringUtil.h"
#include "texture.h"
#include "VertexBuffer.h"
#include "rapidjson\include\rapidjson\rapidjson.h"
#include "rapidjson\include\rapidjson\document.h"
#include <fstream>
#include <sstream>


Game::Game()
{
}

Game::~Game()
{
}

void Game::Init(HWND hWnd, float width, float height)
{
	// TODO Lab 02b
    mGraphics.InitD3D(hWnd, width, height);
    {
        // TODO Lab 02e
        VertexPosColor vert[] =
        {
            { Vector3(0.0f,   0.5f, 0.0f), Graphics::Color4(1.0f, 0.0f, 0.0f, 1.0f) },
            { Vector3(0.45f, -0.5,  0.0f), Graphics::Color4(0.0f, 1.0f, 0.0f, 1.0f) },
            { Vector3(-0.45f, -0.5f, 0.0f), Graphics::Color4(0.0f, 0.0f, 1.0f, 1.0f) }
        };
        uint16_t index[] = { 0, 1, 2 };

        // TODO Lab 02f
        D3D11_INPUT_ELEMENT_DESC inputElem[] =
        {
            { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosColor, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(VertexPosColor, color), D3D11_INPUT_PER_VERTEX_DATA, 0}
        };
        mSimpleShader = new Shader(&mGraphics);
        mSimpleShader->Load(L"Shaders/Mesh.hlsl", inputElem, sizeof(inputElem) / sizeof(inputElem[0]));

        VertexBuffer* pVertBuff = new VertexBuffer(&mGraphics,
            vert, sizeof(vert) / sizeof(vert[0]), sizeof(vert[0]),
            index, sizeof(index) / sizeof(index[0]), sizeof(index[0])
            );
        mTriangle = new RenderObj(this, pVertBuff, mSimpleShader);
    }
    {
        D3D11_INPUT_ELEMENT_DESC inputElem[] =
        {
            { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNormColorUV, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNormColorUV, norm), D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(VertexPosNormColorUV, color), D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(VertexPosNormColorUV, uv), D3D11_INPUT_PER_VERTEX_DATA, 0 }
        };
        mBasicMeshShader = new Shader(&mGraphics);
        mBasicMeshShader->Load(L"Shaders/BasicMesh.hlsl", inputElem, sizeof(inputElem) / sizeof(inputElem[0]));
    }
    mTexture = new Texture(&mGraphics);
    mTexture->Load(L"Assets/Textures/Cube.png");
    mCube = new RenderCube(this, mBasicMeshShader);

    mCamera = new Camera(&mGraphics);
    mLightingBuffer = mGraphics.CreateGraphicsBuffer(&mLightData, sizeof(mLightData), D3D11_BIND_CONSTANT_BUFFER, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);
    SetAmbientLight(Vector3(0.1f, 0.1f, 0.1f));
    {
        PointLightData* pLight1 = AllocateLight();
        pLight1->diffuseColor = Vector3(1.0f, 1.0f, 1.0f);
        pLight1->specularColor = Vector3(1.0f, 1.0f, 1.0f);
        pLight1->position = Vector3(-100.0f, 0.0f, 50.0f);
        pLight1->specularPower = 10.0f;
        pLight1->innerRadius = 20.0f;
        pLight1->outerRadius = 200.0f;
    }
    {
        PointLightData* pLight2 = AllocateLight();
        pLight2->diffuseColor = Vector3(1.0f, 1.0f, 1.0f);
        pLight2->specularColor = Vector3(1.0f, 1.0f, 1.0f);
        pLight2->position = Vector3(75.0f, 150.0f, 100.0f);
        pLight2->specularPower = 10.0f;
        pLight2->innerRadius = 20.0f;
        pLight2->outerRadius = 200.0f;
    }
}

void Game::Shutdown()
{
	// TODO Lab 02b
    mGraphics.CleanD3D();
	// TODO Lab 02e
	// TODO Lab 02f
    delete mSimpleShader;
    delete mBasicMeshShader;
    delete mTexture;
    delete mTriangle;
    delete mCube;
    delete mCamera;
    mLightingBuffer->Release();
}

static float s_angle = 0.0f;
void Game::Update(float deltaTime)
{
    s_angle += Math::Pi * deltaTime;
    mTriangle->mObjectData.c_modelToWorld = Matrix4::CreateScale(300.0f) * Matrix4::CreateRotationZ(s_angle);

    mCube->mObjectData.c_modelToWorld = Matrix4::CreateScale(100.0f)
        * Matrix4::CreateRotationY(s_angle)
        * Matrix4::CreateRotationX(0.25f*s_angle);
}

void Game::RenderFrame()
{
	// TODO Lab 02b
    Graphics::Color4 clearColor(0.0f, 0.2f, 0.4f, 1.0f);
    mGraphics.BeginFrame(clearColor);
    mCamera->SetActive();
    mGraphics.UploadBuffer(mLightingBuffer, &mLightData, sizeof(mLightData));
    mGraphics.GetDeviceContext()->PSSetConstantBuffers(Graphics::CONSTANT_BUFFER_LIGHTING, 1, &mLightingBuffer);
    mTriangle->Draw();
    mTexture->SetActive(Graphics::TEXTURE_SLOT_DIFFUSE);
    mCube->Draw();

    mGraphics.EndFrame();
}

void Game::OnKeyDown(uint32_t key)
{
	m_keyIsHeld[key] = true;
}

void Game::OnKeyUp(uint32_t key)
{
	m_keyIsHeld[key] = false;
}

bool Game::IsKeyHeld(uint32_t key) const
{
	const auto find = m_keyIsHeld.find(key);
	if (find != m_keyIsHeld.end())
		return find->second;
	return false;
}

Game::PointLightData* Game::AllocateLight()
{
    for (int i = 0; i < MAX_POINT_LIGHTS; ++i)
    {
        if (false == mLightData.c_pointLight[i].isEnabled)
        {
            mLightData.c_pointLight[i].isEnabled = true;
            return &mLightData.c_pointLight[i];
        }
    }
    return nullptr;
}

void Game::FreeLight(PointLightData* pLight)
{
    pLight->isEnabled = false;
}

bool Game::LoadLevel(const WCHAR* fileName)
{
	std::ifstream file(fileName);
	if (!file.is_open())
	{
		return false;
	}

	std::stringstream fileStream;
	fileStream << file.rdbuf();
	std::string contents = fileStream.str();
	rapidjson::StringStream jsonStr(contents.c_str());
	rapidjson::Document doc;
	doc.ParseStream(jsonStr);

	if (!doc.IsObject())
	{
		return false;
	}

	std::string str = doc["metadata"]["type"].GetString();
	int ver = doc["metadata"]["version"].GetInt();

	// Check the metadata
	if (!doc["metadata"].IsObject() ||
		str != "itplevel" ||
		ver != 2)
	{
		return false;
	}

	return true;
}