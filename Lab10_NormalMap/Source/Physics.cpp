#include "stdafx.h"
#include "Physics.h"
#include "Components\CollisionComponent.h"

struct TestAABB
{
	Physics::AABB a;
	Physics::AABB b;
	Physics::AABB overlap;
};
const TestAABB testAABB[] =
{
	{
		Physics::AABB(Vector3(0.0f, 0.0f, 0.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(0.0f, 0.0f, 0.0f), Vector3(10.0f, 10.0f, 10.0f)),
		Physics::AABB(Vector3(0.0f, 0.0f, 0.0f), Vector3(10.0f, 10.0f, 10.0f))
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-110.0f, -10.0f, -10.0f), Vector3(-90.0f, 10.0f, 10.0f)),
		Physics::AABB(Vector3(-100.0f, -10.0f, -10.0f), Vector3(-90.0f, 10.0f, 10.0f))
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(90.0f, -10.0f, -10.0f), Vector3(110.0f, 10.0f, 10.0f)),
		Physics::AABB(Vector3(90.0f, -10.0f, -10.0f), Vector3(100.0f, 10.0f, 10.0f))
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-10.0f, -110.0f, -10.0f), Vector3(10.0f, -90.0f, 10.0f)),
		Physics::AABB(Vector3(-10.0f, -100.0f, -10.0f), Vector3(10.0f, -90.0f, 10.0f))
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-10.0f, 90.0f, -10.0f), Vector3(10.0f, 110.0f, 10.0f)),
		Physics::AABB(Vector3(-10.0f, 90.0f, -10.0f), Vector3(10.0f, 100.0f, 10.0f))
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-10.0f, -10.0f, -110.0f), Vector3(10.0f, 10.0f, -90.0f)),
		Physics::AABB(Vector3(-10.0f, -10.0f, -100.0f), Vector3(10.0f, 10.0f, -90.0f))
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-10.0f, -10.0f, 90.0f), Vector3(10.0f, 10.0f, 110.0f)),
		Physics::AABB(Vector3(-10.0f, -10.0f, 90.0f), Vector3(10.0f, 10.0f, 100.0f))
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-120.0f, -10.0f, -10.0f), Vector3(-110.0f, 10.0f, 10.0f)),
		Physics::AABB(Vector3::One, Vector3::Zero)
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(110.0f, -10.0f, -10.0f), Vector3(120.0f, 10.0f, 10.0f)),
		Physics::AABB(Vector3::One, Vector3::Zero)
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-10.0f, -120.0f, -10.0f), Vector3(10.0f, -110.0f, 10.0f)),
		Physics::AABB(Vector3::One, Vector3::Zero)
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-10.0f, 110.0f, -10.0f), Vector3(10.0f, 120.0f, 10.0f)),
		Physics::AABB(Vector3::One, Vector3::Zero)
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-10.0f, -10.0f, -120.0f), Vector3(10.0f, -10.0f, -110.0f)),
		Physics::AABB(Vector3::One, Vector3::Zero)
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::AABB(Vector3(-10.0f, -10.0f, 110.0f), Vector3(10.0f, 10.0f, 120.0f)),
		Physics::AABB(Vector3::One, Vector3::Zero)
	},
};

//---------------------------------------------------------------------------------
struct TestRay
{
	Physics::AABB box;
	Physics::Ray ray;
	bool hit;
	Vector3 point;
};
const TestRay testRay[] =
{
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::Ray(Vector3(-110.0f, 0.0f, 0.0f), Vector3(-90.0f, 0.0f, 0.0f)),
		true, Vector3(-100.0f, 0.0f, 0.0f)
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::Ray(Vector3(0.0f, -110.0f, 0.0f), Vector3(0.0f, -90.0f, 0.0f)),
		true, Vector3(0.0f, -100.0f, 0.0f)
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::Ray(Vector3(0.0f, 0.0f, -110.0f), Vector3(0.0f, 0.0f, -90.0f)),
		true, Vector3(0.0f, 0.0f, -100.0f)
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::Ray(Vector3(110.0f, 0.0f, 0.0f), Vector3(90.0f, 0.0f, 0.0f)),
		true, Vector3(100.0f, 0.0f, 0.0f)
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::Ray(Vector3(0.0f, 110.0f, 0.0f), Vector3(0.0f, 90.0f, 0.0f)),
		true, Vector3(0.0f, 100.0f, 0.0f)
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::Ray(Vector3(0.0f, 0.0f, 110.0f), Vector3(0.0f, 0.0f, 90.0f)),
		true, Vector3(0.0f, 0.0f, 100.0f)
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::Ray(Vector3(-120.0f, 0.0f, 0.0f), Vector3(-110.0f, 0.0f, 0.0f)),
		false, Vector3::Zero
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::Ray(Vector3(0.0f, -120.0f, 0.0f), Vector3(0.0f, -110.0f, 0.0f)),
		false, Vector3::Zero
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::Ray(Vector3(0.0f, 0.0f, -120.0f), Vector3(0.0f, 0.0f, -110.0f)),
		false, Vector3::Zero
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::Ray(Vector3(120.0f, 0.0f, 0.0f), Vector3(110.0f, 0.0f, 0.0f)),
		false, Vector3::Zero
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::Ray(Vector3(0.0f, 120.0f, 0.0f), Vector3(0.0f, 110.0f, 0.0f)),
		false, Vector3::Zero
	},
	{
		Physics::AABB(Vector3(-100.0f, -100.0f, -100.0f), Vector3(100.0f, 100.0f, 100.0f)),
		Physics::Ray(Vector3(0.0f, 0.0f, 120.0f), Vector3(0.0f, 0.0f, 110.0f)),
		false, Vector3::Zero
	},
};

Physics::Physics()
{
}

Physics::~Physics()
{
}

Physics::AABB::AABB()
{
	mMin = Vector3::Zero;
	mMax = Vector3::Zero;
}

Physics::AABB::AABB(Vector3 min, Vector3 max)
{
	mMin = min;
	mMax = max;
}

Physics::AABB::~AABB()
{
}

Physics::Ray::Ray()
{
	mFrom = Vector3::Zero;
	mTo = Vector3::Zero;
}

Physics::Ray::Ray(Vector3 start, Vector3 end)
{
	mFrom = start;
	mTo = end;
}

Physics::Ray::~Ray()
{
}

bool Physics::Intersect(const AABB& a, const AABB& b, AABB* pOverlap)
{
	bool intersect = !(a.mMin.x > b.mMax.x ||
		b.mMin.x > a.mMax.x ||
		a.mMin.y > b.mMax.y ||
		b.mMin.y > a.mMax.y ||
		a.mMin.z > b.mMax.z ||
		b.mMin.z > a.mMax.z
		);

	if (intersect) {
		Math::Min(abs(a.mMax.x), abs(b.mMax.x)) == abs(a.mMax.x) ? pOverlap->mMax.x = a.mMax.x : pOverlap->mMax.x = b.mMax.x;
		Math::Min(abs(a.mMax.y), abs(b.mMax.y)) == abs(a.mMax.y) ? pOverlap->mMax.y = a.mMax.y : pOverlap->mMax.y = b.mMax.y;
		Math::Min(abs(a.mMax.z), abs(b.mMax.z)) == abs(a.mMax.z) ? pOverlap->mMax.z = a.mMax.z : pOverlap->mMax.z = b.mMax.z;
		Math::Min(abs(a.mMin.x), abs(b.mMin.x)) == abs(a.mMin.x) ? pOverlap->mMin.x = a.mMin.x : pOverlap->mMin.x = b.mMin.x;
		Math::Min(abs(a.mMin.y), abs(b.mMin.y)) == abs(a.mMin.y) ? pOverlap->mMin.y = a.mMin.y : pOverlap->mMin.y = b.mMin.y;
		Math::Min(abs(a.mMin.z), abs(b.mMin.z)) == abs(a.mMin.z) ? pOverlap->mMin.z = a.mMin.z : pOverlap->mMin.z = b.mMin.z;
	}

	// AABB collision
	return intersect;
}

bool Physics::Intersect(const Ray& ray, const AABB& box, Vector3* pHitPoint)
{

	float epsilon = 0.001f;
	float tmin = -FLT_MAX; // tmin is intersection distance
	float tmax = FLT_MAX; // max distance ray can travel

	Vector3 distance = ray.mTo - ray.mFrom;
	//all 3 slabs -> distance.x, distance.y, distance.z
	float distCoord;
	float mFromCoord;
	float mMinCoord;
	float mMaxCoord;
	for (int i = 0; i < 3; i++) {
		if (i == 0)
		{
			distCoord = distance.x;
			mFromCoord = ray.mFrom.x;
			mMinCoord = box.mMin.x;
			mMaxCoord = box.mMax.x;
		}
		else if (i == 1)
		{
			distCoord = distance.y;
			mFromCoord = ray.mFrom.y;
			mMinCoord = box.mMin.y;
			mMaxCoord = box.mMax.y;
		}
		else if (i == 2)
		{
			distCoord = distance.z;
			mFromCoord = ray.mFrom.z;
			mMinCoord = box.mMin.z;
			mMaxCoord = box.mMax.z;
		}
		if (abs(distCoord) < epsilon)
		{
			// Ray is parallel to slab. No hit if origin not within slab
			if (mFromCoord < mMinCoord || mFromCoord > mMaxCoord)
			{
				return false;
			}
		}
		else // Compute intersection t value of ray with near and far plane of slab
		{
			float ood = 1.0f / distCoord;
			float t1 = (mMinCoord - mFromCoord) * ood;
			float t2 = (mMaxCoord - mFromCoord) * ood;
			// Make t1 be intersection with near plane, t2 with far plane
			if (t1 > t2)
			{
				float temp = t1;
				t1 = t2;
				t2 = temp;
			}
			// Compute the intersection of slab intersection intervals
			tmin = max(tmin, t1); //if (t1 > tmin) tmin = t1;
			tmax = min(tmax, t2); //if (t2 > tmax) tmax = t2;
			// Exit with no collision as soon as slab intersection becomes empty
			if (tmin > tmax)
			{
				return false;
			}
		}
	}
	//clamp line segment between t = 0 and t = 1 - character was falling because returned t was likely negative or passed the line segment
	if (tmin > 1.00 || tmax < 0.0)
	{
		return false;
	}
	// Ray intersects all 3 slabs. Return point (q) and intersection t value (tmin)
	if (pHitPoint != nullptr)
	{
		*pHitPoint = ray.mFrom + distance * tmin;
	}

	return true;
}

bool Physics::UnitTest()
{

	bool isOk = true;

	// intersect AABB vs AABB
	{
		for (unsigned int i = 0; i < sizeof(testAABB) / sizeof(testAABB[0]); i++)
		{
			AABB overlap;
			Intersect(testAABB[i].a, testAABB[i].b, &overlap);
			//isOk &= Intersect(testAABB[i].a, testAABB[i].b, &overlap);
			isOk &= Math::IsCloseEnuf(overlap.mMin.x, testAABB[i].overlap.mMin.x);
			isOk &= Math::IsCloseEnuf(overlap.mMin.y, testAABB[i].overlap.mMin.y);
			isOk &= Math::IsCloseEnuf(overlap.mMin.z, testAABB[i].overlap.mMin.z);
			isOk &= Math::IsCloseEnuf(overlap.mMax.x, testAABB[i].overlap.mMax.x);
			isOk &= Math::IsCloseEnuf(overlap.mMax.y, testAABB[i].overlap.mMax.y);
			isOk &= Math::IsCloseEnuf(overlap.mMax.z, testAABB[i].overlap.mMax.z);
		}
		return isOk;
	}

	// intersect ray
	{
		for (unsigned int i = 0; i < sizeof(testRay) / sizeof(testRay[0]); i++)
		{
			Vector3 hitPoint;
			bool intersect = Intersect(testRay[i].ray, testRay[i].box, &hitPoint);
			isOk &= (intersect == testRay[i].hit);
			isOk &= Math::IsCloseEnuf(hitPoint.x, testRay[i].point.x);
			isOk &= Math::IsCloseEnuf(hitPoint.y, testRay[i].point.y);
			isOk &= Math::IsCloseEnuf(hitPoint.z, testRay[i].point.z);
		}
		return isOk;
	}
}

void Physics::AddObj(CollisionComponent* pObj)
{
	mObj.push_back(pObj);
}

void Physics::RemoveObj(CollisionComponent* pObj)
{
	std::vector<CollisionComponent*>::iterator it = find(mObj.begin(), mObj.end(), pObj);
	if (it != mObj.end())
	{
		mObj.erase(it);
	}
}

bool Physics::RayCast(const Physics::Ray& ray, Vector3* pHitPoint)
{
	/*
	Loop through mObj
	o For each one, use GetAABB()
	o For each one call Physics::Intersect() (the Ray vs AABB one)
	o Among all AABBs that do intersect the Ray, return the point closest to the beginning of the Ray.
	*/
	//set closest to arbitrary high value
	float closest = INFINITY;
	bool intersect = false;
	for (unsigned int i = 0; i < mObj.size(); i++)
	{
		AABB box = mObj[i]->GetAABB();
		Vector3 hitPoint;
		if (Intersect(ray, box, &hitPoint) == true)
		{
			intersect = true;

			//get distance from box to ray
			//hitpoint is point on box
			float distance = (hitPoint - ray.mFrom).Length();
			//if distance less than current closest
			//closest set to distance
			//pHitPoint set to hitPoint
			if (distance < closest)
			{
				closest = distance;
				*pHitPoint = hitPoint;
			}
		}
	}
	return intersect;
}