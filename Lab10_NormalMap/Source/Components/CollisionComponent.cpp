﻿#include "stdafx.h"
#include "CollisionComponent.h"
#include "game.h"
#include "jsonUtil.h"
#include "RenderObj.h"

CollisionComponent::CollisionComponent(RenderObj* pObj)
	: Component(pObj)
{
	mObj->GetGame()->mPhysics.AddObj(this);
}

CollisionComponent::~CollisionComponent()
{
	mObj->GetGame()->mPhysics.RemoveObj(this);
}

void CollisionComponent::LoadProperties(const rapidjson::Value& properties)
{
	/*"position": [0.0, 650.0, 0.0],
		"rotation" : [0.0, 0.0, 0.0, 1.0],
		"scale" : 5.0,
		"mesh" : "Assets/Meshes/Platform.itpmesh2",
		"components" : [
	{
		"type": "Collision",
			"min" : [-50.0, -50.0, -1.0],
			"max" : [50.0, 50.0, 0.0]
	}*/
	Vector3 min;
	Vector3 max;
	GetVectorFromJSON(properties, "min", min);
	GetVectorFromJSON(properties, "max", max);
	mAABB.mMin = min;
	mAABB.mMax = max;
}

Physics::AABB CollisionComponent::GetAABB() const
{
	/*
	Apply the scale of the RenderObj to the AABB
	▪ We don’t support non-uniform scale, so you can just take the x component of
	the scale vector.
	o Apply the translation of the RenderObj to the AABB.
	o For our purposes, let’s ignore the rotation of the RenderObj.
	o It should go without saying, but do not modify the mAABB itself. Make a copy and scale
	and translate that before returning it
	*/

	// Make a copy of mAABB; scale and translate that before returning it
	Physics::AABB aabb = mAABB;
	// Apply the scale of the RenderObj to the AABB
	float scale = mObj->mObjectData.c_modelToWorld.GetScale().x;
	// Apply the translation of the RenderObj to the AABB
	Vector3 translation = mObj->mObjectData.c_modelToWorld.GetTranslation();

	// Update mMin and mMax
	aabb.mMin = aabb.mMin * scale + translation;
	aabb.mMax = aabb.mMax * scale + translation;

	return aabb;
}