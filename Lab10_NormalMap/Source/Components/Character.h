#pragma once
#include "Component.h"

class Animation;
class RenderObj;
class Skeleton;
class SkinnedObj;

class Character : public Component
{
public:
    Character(SkinnedObj* pObj);
    void LoadProperties(const rapidjson::Value& properties) override;
    bool SetAnim(const std::string& animName);
    void UpdateAnim(float deltaTime);
    void Update(float deltaTime) override;

	// TODO Lab 08f
	//static Character* Get();
	float GetAnimTime() { return mAnimationTime; }
	const Animation* GetCurrAnim() { return mCurrentAnim; }
	Skeleton* GetSkeleton() { return mSkeleton; }
	SkinnedObj* GetSkinnedObj() { return mSkinnedObj; }
	void SetSkinned(SkinnedObj*skin) { mSkinnedObj = skin; }

protected:
    SkinnedObj* mSkinnedObj;
    Skeleton* mSkeleton;
    std::unordered_map<std::string, const Animation*> mAnims;
    const Animation* mCurrentAnim;
    float mAnimationTime;

	// TODO Lab 08f
	static SkinnedObj* skinned;
	static Skeleton* pSkeleton;
	static const Animation* currAnim;
	static float animTime;
};