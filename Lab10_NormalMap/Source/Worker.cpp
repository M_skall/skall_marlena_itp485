#include "stdafx.h"
#include "Worker.h"
#include "JobManager.h"
#include "Job.h"
#include <chrono>

Worker::Worker()
{

}

Worker::~Worker()
{

}

void Worker::Begin()
{
	/*mThread = std::thread([&]() {
		Loop();
	});*/

	mThread = std::thread(Loop);

}

void Worker::End()
{
	mThread.join();
}

void Worker::Loop()
{
	while (JobManager::GetShutdownSignal())
	{
		JobManager::GetMutex().lock();
		std::queue<Job*> jobQueue = JobManager::GetJobQueue();
		if (!jobQueue.empty())
		{
			Job* job = JobManager::GetJobQueue().front();
			JobManager::GetJobQueue().pop();
			JobManager::GetMutex().unlock();
			job->DoIt();
			JobManager::mJobCount--;
			delete job;
		}
		else 
		{

			JobManager::GetMutex().unlock();
			Sleep(1);
		}
	}
}