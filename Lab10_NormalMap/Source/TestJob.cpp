#include "stdafx.h"
#include "game.h"
#include "Components/Character.h"
#include "jsonUtil.h"
#include "RenderObj.h"
#include "Animation.h"
#include "Skeleton.h"
#include "SkinnedObj.h"
#include "stringUtil.h"
#include "TestJob.h"
#include "JobManager.h"
#include <iostream>

TestJob::TestJob(Character* character)
	: Job()
{
	mCharacter = character;
}

TestJob::~TestJob()
{

}

void TestJob::DoIt()
{
	std::vector<Matrix4> outPoses;

	const Animation* currAnim = mCharacter->GetCurrAnim();
	Skeleton* skeleton = mCharacter->GetSkeleton();
	SkinnedObj* skinnedObj = mCharacter->GetSkinnedObj();
	float animTime = mCharacter->GetAnimTime();
	currAnim->GetGlobalPoseAtTime(outPoses, skeleton, animTime);

	for (unsigned int i = 0; i < skeleton->GetNumBones(); ++i)
	{
		skinnedObj->mSkinConstants.c_skinMatrix[i] = skeleton->GetGlobalInvBindPoses()[i] * outPoses[i];
	}
	mCharacter->SetSkinned(skinnedObj);
}