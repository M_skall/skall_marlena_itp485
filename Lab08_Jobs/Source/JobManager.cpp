#include "stdafx.h"
#include "Worker.h"
#include "Job.h"
#include "JobManager.h"

bool JobManager::mRun = true;
std::mutex JobManager::queue_mutex;
std::queue<Job*> JobManager::mJobQueue;
std::atomic<int> JobManager::mJobCount = 0;

JobManager::JobManager()
{
	for (unsigned int i = 0; i < NUM_WORKERS; i++)
	{
		mWorkers[i] = new Worker();
	}
}

JobManager::~JobManager()
{
	for (unsigned int i = 0; i < NUM_WORKERS; i++)
	{
		delete mWorkers[i];
	}
}

void JobManager::Begin()
{
	mRun = true;
	for (unsigned int i = 0; i < NUM_WORKERS; i++)
	{
		mWorkers[i]->Begin();
	}
}

void JobManager::End()
{
	mRun = false;
	for (unsigned int i = 0; i < NUM_WORKERS; i++)
	{
		mWorkers[i]->End();
	}
}

void JobManager::AddJob(Job* job)
{
	{
		queue_mutex.lock();
		mJobQueue.push(job);
		mJobCount++;
		queue_mutex.unlock();
	}
}

// count up jobs to know when they are done
void JobManager::WaitForJobs()
{

	while (mJobCount > 0)
	{
		Sleep(1);
	}
}