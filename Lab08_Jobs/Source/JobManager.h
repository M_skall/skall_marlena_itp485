#pragma once
#include <queue>
#include <deque>
#include <mutex>
#include <atomic>

#define NUM_WORKERS 4

class Worker;
class Job;

class JobManager
{
public:
	JobManager();
	~JobManager();
	void Begin(); // begin each Worker
	void End(); // set shutdown signal and end() each Worker
	void AddJob(Job* job);
	void WaitForJobs();

	static bool GetShutdownSignal() { return mRun; }
	static std::queue<Job*>& GetJobQueue() { return mJobQueue; }
	//static std::deque<Job*> GetJobDeque() { return mJobDeque; }
	static std::mutex& GetMutex() { return queue_mutex; }
	static void DecrementCount() { mJobCount--; }
	static int GetJobCount() { return mJobCount; }

	friend class Worker;

private:
	static bool mRun;

	Worker* mWorkers[NUM_WORKERS];

	//potentially make it an array?
	//can't forward declare if not a pointer
	Worker* worker1;
	Worker* worker2;
	Worker* worker3;
	Worker* worker4;

	// TODO Lab 08e
	//need simple FIFO collection of <Job*>
	static std::queue<Job*> mJobQueue;
	static std::deque<Job*> mJobDeque;
	//need mutex so only one thread can access at a time
	static std::mutex queue_mutex;
	std::mutex deque_mutex;

	static std::atomic<int> mJobCount;
};